import React, { Component } from 'react';
import { Route, Redirect, Switch } from "react-router-dom";
import "semantic-ui-css/semantic.css";
import { connect } from 'react-redux';
import { NotificationContainer } from 'react-notifications';
import './App.css';
import 'react-notifications/lib/notifications.css';
import Login from './Components/Shared/Login';
import ConfirmPassword from './Components/Shared/ConfirmPassword';
import ForgotPassword from './Components/Shared/ForgotPassword';
import { alreadyLoggedIn } from './state/actions/loginActions';
import ContentPanel from './Components/Shared/ContentPanel/ContentPanel';
import { Actions } from './state/actions/menuActions';

class App extends Component {
    render() {
	    let token = sessionStorage.getItem("access_token");
		let user = sessionStorage.getItem("userid");
		
	    if (token && user && !this.props.user && !this.props.isLoggingIn) {
		    this.props.alreadyLoggedIn();
		}

		if (this.props.redirectAdmin && !window.location.hash.endsWith('/admin')) {
            return <Redirect to={"/admin"} />;
        } else if (this.props.redirectUser && !window.location.hash.endsWith('/user')) {
            return <Redirect to={"/user"} />;
		} else if (this.props.tokenBad) {
			return <Redirect to={"/"} />;
		}
		
        return (
            <div style={{flex: 1}}>
				<NotificationContainer/>
				<Switch>
					<Route path={`/admin`} render={() => <ContentPanel panelType='admin' activeTab={this.props.activeTab} menuOpen={this.props.menuOpen} navigateTab={this.props.navigateTab} />}/>
					<Route path={`/user`} render={() => <ContentPanel panelType='user' activeTab={this.props.activeTab} menuOpen={this.props.menuOpen} navigateTab={this.props.navigateTab} />}/>
					<Route path={`/forgotpassword`} component={ForgotPassword}/>
					<Route path={`/passwordreset`} component={ConfirmPassword}/>
					<Route exact path={`/`} component={Login}/>
				</Switch>
            </div>
        );
    }
}

const mapStateToProps = state => ({
	...state.login,
	...state.menu
});

const mapDispatchToProps = dispatch => {
	return {
		alreadyLoggedIn: () => { dispatch(alreadyLoggedIn()) },
		navigateTab: (tabKey) => { dispatch(Actions.navigateTab(tabKey)) }
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(App);