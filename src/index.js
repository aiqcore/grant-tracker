import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route } from 'react-router-dom';
import App from './App';
import { Provider } from 'react-redux';
import { createAppStore } from './state/stores/createAppStore';
import registerServiceWorker from './registerServiceWorker';

import './index.css';
import "semantic-ui-css/semantic.css";
import 'react-notifications/lib/notifications.css';

const store = createAppStore();

ReactDOM.render(
    <Provider store={store}>
        <HashRouter>
            <Route path="/" component={App} />
        </HashRouter>
    </Provider>
    , document.getElementById('root')
);
registerServiceWorker();
