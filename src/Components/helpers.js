export function cleanObject(inputObject) {
    for (let key of Object.keys(inputObject)) {
        if (inputObject[key] === "") {
            delete inputObject[key];
        }
    }
    return inputObject;
}