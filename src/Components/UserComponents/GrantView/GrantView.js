import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchGrants, Actions } from '../../../state/actions/userActions';
import { Spin } from 'antd';
import './GrantView.css';

import Layout from '../../Shared/Layout/Layout';
import GrantButtonPanel from '../GrantButtonPanel/GrantButtonPanel';
import TransactionTable from "../TransactionTable/TransactionTable";
import ChartPanel from '../ChartPanel/ChartPanel';

class GrantView extends Component {
    componentDidMount() {
        this.props.fetchGrants(sessionStorage.getItem('userid'));
    }

    render() {
        return (
            <div className="content-div">
                <Layout title="My Grants" description="View grant balances and transaction histories." />
                {
                    this.props.grants.length === 0 ?
                    <Spin className="loader" size="large" /> :
                    <div>
                        <GrantButtonPanel 
                            grants={this.props.grants}
                            grantSelectedIndex={this.props.grantSelectedIndex} 
                            changeSelectedGrant={this.props.changeSelectedGrant} />
                        <hr />
                        <ChartPanel />
                        <TransactionTable 
                            grants={this.props.grants} 
                            grantSelectedIndex={this.props.grantSelectedIndex} />
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = state => (
    {
        ...state.user,
        user: state.login.user,
    }
);

const mapDispatchToProps = dispatch => (
    {
        fetchGrants: (user) => { dispatch(fetchGrants(user)) },
        changeSelectedGrant: (grantIndex) => { dispatch(Actions.changeSelectedGrant(grantIndex)) }
    }
);

export default connect(mapStateToProps, mapDispatchToProps)(GrantView);