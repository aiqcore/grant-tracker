import React,{ Component } from 'react';
import {connect} from "react-redux";
import { Row, Col, Card } from 'antd';
import { ResponsiveContainer, Cell, Tooltip, PieChart, Pie, Legend } from 'recharts';
import moment from "moment/moment";
import { EXPENSE_TYPES } from "../../../state/config";
import './ChartPanel.css';
import ChartBalanceOverTime from '../ChartBalanceOverTime/ChartBalanceOverTime';

const COLORS = ["GREEN", "LIGHTCORAL", "SLATE", "SKYBLUE", "RED", "BLACK", "GRAY", "GOLD", "PURPLE"];
const CHART_HEIGHT = 300;

class ChartPanel extends Component {
    determineQuarters(grant) {
        const QUARTER_MONTHS = new Set([0, 3, 6, 9]);
        let format = 'YYYY-MM-DD';

        let endDate = moment(grant['end_date'], format);
        let startCounter = moment(grant['start_date'], format);
        let quarters = [];

        while (startCounter.isBefore(endDate)) {
            startCounter.add(1, 'days');
            if (QUARTER_MONTHS.has(startCounter.get('month')) && startCounter.get('date') === 1) {
                quarters.push(new Date(startCounter.valueOf()));
            }
        }
        return quarters;
    }

    populateLargePie() {
        let categoryCount = {};
        let data = [];
        let grant = this.props.grants[this.props.grantSelectedIndex];

        data.push({"name": "Unused", "value": parseFloat(grant['budget'] - grant['sumOfExpenses'].toFixed(2))});

        EXPENSE_TYPES.forEach(type => categoryCount[type] = 0);

        grant['transactions'].forEach(trans => {
            categoryCount[trans['category']] = categoryCount[trans['category']] + trans['amount'];
        });

        Object.keys(categoryCount).forEach((key) => {
            if (categoryCount[key] !== 0) {
                data.push({
                    "name": key, "value": parseFloat(categoryCount[key].toFixed(2))
                })
            }
        });

        data.sort((a, b) => {
            return a.name < b.name
        });

        return data;
    }

    render() {
        let grant = this.props.grants[this.props.grantSelectedIndex];

        let quarters = this.determineQuarters(grant);

        let largePieData = this.props.grants.length > 0 ? this.populateLargePie() : null;
        
        let transByDate = grant['transactions'].sort((a, b) => {
            return new Date(a['date_paid']) - new Date(b['date_paid']);
        });

        return (
            <Row gutter={16}>
                <h2 className="chart-header">Charts</h2>
                <Col xs={24} sm={24} md={18}>
                    <ChartBalanceOverTime data={transByDate} grant={grant} quarters={quarters} />
                </Col>

                <Col span={6} xs={24} sm={24} md={6}>
                    <Card title="Funds Allocation">
                        <ResponsiveContainer height={CHART_HEIGHT} width="100%">
                            <PieChart>
                                <Legend />
                                <Pie data={largePieData} dataKey="value" nameKey="name" cx="50%" cy="50%"
                                     outerRadius={80} paddingAngle={1}
                                     label={(name) => "$" + name.value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
                                     labelLine={false}>
                                    {largePieData.map((entry, index) =>  entry['value'] > 0 ? <Cell fill={COLORS[index]} key={index}/> : null)}
                                </Pie>
                                <Tooltip formatter={(value) => "$" + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')} />
                            </PieChart>
                        </ResponsiveContainer>
                    </Card>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    ...state.user
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ChartPanel);