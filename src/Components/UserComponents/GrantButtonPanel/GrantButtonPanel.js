import React from 'react';
import PropTypes from 'prop-types';
import { Row } from 'antd';
import { Element } from 'react-scroll';

import GrantButton from '../GrantButton/GrantButton';

export default function GrantButtonPanel({ grants, grantSelectedIndex, changeSelectedGrant }) {
    let grantList = Object.values(grants).map((grant, grantIndex) => {
        return (
            <GrantButton 
                key={grantIndex}
                grant={grant} 
                grantIndex={grantIndex} 
                grantSelectedIndex={grantSelectedIndex} 
                changeSelectedGrant={changeSelectedGrant} />
        );
    });

    return (
        <Element style={{height: '220px', overflowY: 'scroll', overflowx: 'hidden'}}>
            <Row className="grant-list" gutter={{ md: 8, lg: 24}} style={{ width: '100%'}}>
                { grantList }
            </Row>
        </Element>
    );
}

GrantButtonPanel.propTypes = {
    grants: PropTypes.arrayOf(PropTypes.shape({
        grant_name: PropTypes.string,
        budget: PropTypes.number.isRequired,
        sumOfExpenses: PropTypes.number.isRequired,
        start_date: PropTypes.string.isRequired,
        end_date: PropTypes.string.isRequired,
        principal_investigator: PropTypes.string.isRequired
    })).isRequired,
    grantSelectedIndex: PropTypes.number.isRequired,
    changeSelectedGrant: PropTypes.func.isRequired
}

GrantButtonPanel.defaultProps = {
    grantSelectedIndex: 0
}