import React from 'react';
import PropTypes from 'prop-types';
import Card from 'antd/lib/card';
import ChartContainer from 'react-timeseries-charts/lib/components/ChartContainer';
import ChartRow from 'react-timeseries-charts/lib/components/ChartRow';
import Charts from 'react-timeseries-charts/lib/components/Charts';
import AreaChart from 'react-timeseries-charts/lib/components/AreaChart';
import LineChart from 'react-timeseries-charts/lib/components/LineChart';
import YAxis from 'react-timeseries-charts/lib/components/YAxis';
import TimeMarker from 'react-timeseries-charts/lib/components/TimeMarker';
import Resizable from 'react-timeseries-charts/lib/components/Resizable';
import Legend from 'react-timeseries-charts/lib/components/Legend';
import Brush from 'react-timeseries-charts/lib/components/Brush';
import TimeRange from 'pondjs/lib/lib/timerange';
import TimeSeries from 'pondjs/lib/lib/timeseries';
import moment from 'moment';

import './ChartBalanceOverTime.css';

const format = 'YYYY-MM-DD';

const chartStyle = {
    balanceRemaining: {
        line: {
            normal: {stroke: "#0085ad", fill: "none", strokeWidth: 1},
            highlighted: {stroke: "#5a98cb", fill: "none", strokeWidth: 1},
            selected: {stroke: "#0085ad", fill: "none", strokeWidth: 1},
            muted: {stroke: "#0085ad", fill: "none", opacity: 0.4, strokeWidth: 1}
        },
        area: {
            normal: {fill: "#0085ad80", stroke: "none", opacity: 0.75},
            highlighted: {fill: "#5a98cb", stroke: "none", opacity: 0.75},
            selected: {fill: "#0085ad", stroke: "none", opacity: 0.75},
            muted: {fill: "#0085ad", stroke: "none", opacity: 0.25}
        }
    },
    optimalSpending: {
        normal: {stroke: "#018001", fill: "none", strokeWidth: 2},
        highlighted: {stroke: "#5a98cb", fill: "none", strokeWidth: 1},
        selected: {stroke: "#0085ad", fill: "none", strokeWidth: 1},
        muted: {stroke: "#0085ad", fill: "none", opacity: 0.4, strokeWidth: 1}
    }
};

const legendStyle = {
    optimalSpending: {
        symbol: {
            normal: { stroke: "#018001", fill: "none", strokeWidth: 2, display: 'block', margin: 'auto' },
            highlighted: { stroke: "#5a98cb", fill: "none", strokeWidth: 1 },
            selected: { stroke: "#0085ad", fill: "none", strokeWidth: 2 },
            muted: { stroke: "#0085ad", fill: "none", opacity: 0.4, strokeWidth: 1 }
        },
        label: {
            normal: { fontSize: "normal", color: "#333" },
            highlighted: { fontSize: "normal", color: "#222" },
            selected: { fontSize: "normal", color: "#333" },
            muted: { fontSize: "normal", color: "#333", opacity: 0.4 }
        },
        value: {
            normal: { fontSize: "normal", color: "#333" },
            highlighted: { fontSize: "normal", color: "#222" },
            selected: { fontSize: "normal", color: "#333" },
            muted: { fontSize: "normal", color: "#333", opacity: 0.4 }
        }
    },
    balanceRemaining: {
        symbol: {
            normal: { stroke: "#0085ad", fill: "#0085ad", strokeWidth: 1 },
            highlighted: { stroke: "#5a98cb", fill: "none", strokeWidth: 1 },
            selected: { stroke: "#0085ad", fill: "none", strokeWidth: 2 },
            muted: { stroke: "#0085ad", fill: "none", opacity: 0.4, strokeWidth: 1 }
        },
        label: {
            normal: { fontSize: "normal", color: "#333" },
            highlighted: { fontSize: "normal", color: "#222" },
            selected: { fontSize: "normal", color: "#333" },
            muted: { fontSize: "normal", color: "#333", opacity: 0.4 }
        },
        value: {
            normal: { fontSize: "normal", color: "#333" },
            highlighted: { fontSize: "normal", color: "#222" },
            selected: { fontSize: "normal", color: "#333" },
            muted: { fontSize: "normal", color: "#333", opacity: 0.4 }
        }
    },
    quarter: {
        symbol: {
            normal: { stroke: "black", fill: "none", strokeWidth: 1, strokeDasharray: '5, 5' },
            highlighted: { stroke: "#5a98cb", fill: "none", strokeWidth: 1 },
            selected: { stroke: "#0085ad", fill: "none", strokeWidth: 2 },
            muted: { stroke: "#0085ad", fill: "none", opacity: 0.4, strokeWidth: 1 }
        },
        label: {
            normal: { fontSize: "normal", color: "#333" },
            highlighted: { fontSize: "normal", color: "#222" },
            selected: { fontSize: "normal", color: "#333" },
            muted: { fontSize: "normal", color: "#333", opacity: 0.4 }
        },
        value: {
            normal: { fontSize: "normal", color: "#333" },
            highlighted: { fontSize: "normal", color: "#222" },
            selected: { fontSize: "normal", color: "#333" },
            muted: { fontSize: "normal", color: "#333", opacity: 0.4 }
        }
    }
};

const legend = [
    {
        key: 'optimalSpending',
        label: 'Optimal Spending Trajectory',
        disabled: false,
        symbolType: 'line'
    },
    {
        key: 'balanceRemaining',
        label: 'Spent',
        disabled: false
    },
    {
        key: 'quarter',
        label: 'Fiscal Quarter Beginning',
        disabled: false,
        symbolType: 'line'
    }
];

export default function ChartBalanceOverTime({ data, grant, quarters }) {
    let timeRange = new TimeRange(moment(grant['start_date'], format), moment(grant['end_date'], format));
    let balance = grant['budget'];

    let optimalSpendingSeries = new TimeSeries({
        name: 'optimalSpending',
        columns: ['time', 'optimalSpending'],
        points: [
            [moment(grant['start_date'], format).valueOf(), grant['budget']],
            [moment(grant['end_date'], format).valueOf(), 0]
        ]
    });

    let series = {
        name: 'balanceOverTime',
        columns: ['time', 'balanceRemaining'],
        points: []
    };

    for (let dataPoint of data) {
        series['points'].push([moment(dataPoint['date_paid'], format).valueOf(), balance - dataPoint['amount']]);
        balance = balance - dataPoint['amount'];
    }

    let timeSeries = new TimeSeries(series);

    let quarterMarkers = quarters.map(quarter => <TimeMarker key={quarter} time={quarter} infoStyle={{line: { stroke: 'black', strokeDasharray: '5, 5' }}} />);

    return (
        <Card title={`Balance Over Time`} id='line-graph-card'>
            <Legend 
                className="line-graph-legend"
                categories={legend}
                style={legendStyle}
            />
            <Resizable>
                <ChartContainer timeRange={timeRange} enablePanZoom enableDragZoom>
                    <ChartRow height='200'>
                        <YAxis id='balance' label='Balance' min={0} max={grant['budget']} />
                        <Charts>
                            <LineChart 
                                series={optimalSpendingSeries}
                                axis='balance'
                                style={chartStyle}
                                columns={['optimalSpending']}
                            />
                            <AreaChart 
                                series={timeSeries} 
                                axis='balance' 
                                columns={{up: ["balanceRemaining"]}} 
                                style={chartStyle}
                                interpolation='curveStepBefore' />
                            { quarterMarkers }
                        </Charts>
                    </ChartRow>
                </ChartContainer>
            </Resizable>
        </Card>
    );
}

ChartBalanceOverTime.propTypes = {
    data: PropTypes.array.isRequired,
    grant: PropTypes.object.isRequired,
    quarters: PropTypes.array.isRequired
}
