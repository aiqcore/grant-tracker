import React from 'react';
import PropTypes from 'prop-types';
import { Table, Col } from 'antd';

import { EXPENSE_TYPES } from "../../../state/config";

import './TransactionTable.css';

let categoryFilter = EXPENSE_TYPES.map(type => {
    return {text: type, value: type}
});

const columns = [
    {
        title: 'Category',
        dataIndex: 'category',
        filters: categoryFilter,
        filterMultiple: true,
        onFilter: (value, record) => record.category.indexOf(value) === 0,
    },
    {
        title: 'Paid To',
        dataIndex: 'paid_to'
    },
    {
        title: 'Date',
        dataIndex: 'date_paid',
        defaultSortOrder: 'descend',
        sorter: (a, b) => Date.parse(a.date_paid) - Date.parse(b.date_paid),
    },
    {
        title: 'Amount',
        dataIndex: 'amount',
        sorter: (a, b) => a.amount - b.amount,
    }
];

function populateTransactions(grants, grantSelectedIndex) {
    let trans = grants[grantSelectedIndex].transactions;
    trans = trans.map((trans, transIndex) => {
        return {...trans, amount: "$" + trans.amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'), key: transIndex}
    });
    return trans;
}

export default function TransactionTable({ grants, grantSelectedIndex }) {
    return (
        <div className="transaction-container">
            <h2 className="transaction-header">Transaction History</h2>
            <Table columns={columns} dataSource={ populateTransactions(grants, grantSelectedIndex) }
                    size="small"
                    expandedRowRender={record => (
                        <div>
                            <Col sm={18} md={12}>
                                <h4 className="transaction-subhead">Notes</h4>
                                <p style={{ margin: 0 }}>{record['misc_note']}</p>
                            </Col>
                            <Col sm={6} md={12}>
                                <h4 className="transaction-subhead">Authorizer</h4>
                                <p>{record["authorizer_name"]}</p>
                            </Col>
                        </div>
                    )} />
        </div>
    )
}

TransactionTable.propTypes = {
    grants: PropTypes.arrayOf(PropTypes.shape({
        grant_name: PropTypes.string,
        budget: PropTypes.number.isRequired,
        sumOfExpenses: PropTypes.number.isRequired,
        start_date: PropTypes.string.isRequired,
        end_date: PropTypes.string.isRequired,
        principal_investigator: PropTypes.string.isRequired
    })).isRequired,
    grantSelectedIndex: PropTypes.number.isRequired,
}

TransactionTable.defaultProps = {
    grantSelectedIndex: 0
}