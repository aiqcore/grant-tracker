import React from 'react';
import PropTypes from 'prop-types';
import { Card, Col, Progress } from 'antd';

import './GrantButton.css';

const ACTIVE_GRANT_CLASS = "grant-card grant-active";
const INACTIVE_GRANT_CLASS = "grant-card";

export default function Button({ grant, grantSelectedIndex, changeSelectedGrant, grantIndex }) {
    let grantName = grant["grant_name"];
    let grantBalance = (grant["budget"] - grant["sumOfExpenses"]).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    let percentRemaining = Math.round((1 - (grant["sumOfExpenses"] / grant["budget"])) * 100);
    let startDate = grant["start_date"];
    let endDate = grant["end_date"];
    let principalInvestigator = grant['principal_investigator'];

    let displayClass = startDate !== null && endDate !== null ? "" : "hidden";

    return (
        <Col key={grantIndex} xl={8} lg={12} md={24} sm={24} xs={24}>
            <Card onClick={() => changeSelectedGrant(grantIndex)}
                    className={ grantIndex === grantSelectedIndex ? ACTIVE_GRANT_CLASS : INACTIVE_GRANT_CLASS }>
                    <h4 className="grant-title">{grantName}</h4>
                    <div className="grant-balance">${grantBalance}</div>
                    <div className="balance-description">Balance Remaining</div>
                    <div className={displayClass}><small>{startDate + " | " + endDate}</small></div>
                    <small className="principal-investigator"><strong>Principal Investigator:</strong> {principalInvestigator}</small>
                    <Progress percent={percentRemaining} />
            </Card>
        </Col>
    )
}

Button.propTypes = {
    grant: PropTypes.shape({
        grant_name: PropTypes.string,
        budget: PropTypes.number.isRequired,
        sumOfExpenses: PropTypes.number.isRequired,
        start_date: PropTypes.string.isRequired,
        end_date: PropTypes.string.isRequired,
        principal_investigator: PropTypes.string.isRequired
    }).isRequired,
    grantSelectedIndex: PropTypes.number.isRequired,
    changeSelectedGrant: PropTypes.func.isRequired,
    grantIndex: PropTypes.number.isRequired
};