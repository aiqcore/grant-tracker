import React, { Component } from 'react';
import { Row, Col } from 'antd';
import { connect } from "react-redux";

import CreateButtonContainer from '../CreateButtonContainer/CreateButtonContainer';
import Layout from '../../Shared/Layout/Layout';

import { Actions } from '../../../state/actions/modalActions';
import { fetchSalutations } from "../../../state/actions/adminActions";
import { fetchInstitutions, fetchInvestigators } from '../../../state/actions/grantActions';

import './AdminManager.css';

class AdminManager extends Component {

	showUserModalAndLoadData = () => {
		this.props.showUserModal();
		if (this.props.salutations.length === 0) {
			this.props.fetchSalutations();
		}
	}

	showGrantModalAndLoadData = () => {
		this.props.showGrantModal();
		this.props.fetchInvestigators();
		if (this.props.institutions.length === 0) {
			this.props.fetchInstitutions();
		}
	}

	render() {
		return (
			<div className="admin-container">
				<Layout title="Manage Records" description="Use this panel to create, edit and delete data from the application." />
				<Row className="create-button-panel" gutter={10}>
					<Col md={8} xs={24}>
						<CreateButtonContainer 
							type="user"
							showModal={this.showUserModalAndLoadData}
							modalVisible={this.props.modal.userModalVisible}
							handleCancel={this.props.closeModal}
						/>
					</Col>

					<Col md={8} xs={24}>
						<CreateButtonContainer 
							type="grant"
							showModal={this.showGrantModalAndLoadData}
							modalVisible={this.props.modal.grantModalVisible}
							handleCancel={this.props.closeModal} />
					</Col>
					<Col md={8} xs={24}>
						<CreateButtonContainer
							type="transaction"
							showModal={this.props.showTransactionModal}
							modalVisible={this.props.modal.transactionModalVisible}
							handleCancel={this.props.closeModal} />
					</Col>
				</Row>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	...state.admin,
	...state.grant,
	modal: state.modal
});

const mapDispatchToProps = dispatch => ({
	showUserModal: () => { dispatch(Actions.showUserModal()) },
	showGrantModal: () => { dispatch(Actions.showGrantModal()) },
	showTransactionModal: () => { dispatch(Actions.showTransactionModal()) },
	closeModal: () => { dispatch(Actions.closeModals()) },
	fetchSalutations: () => { dispatch(fetchSalutations()) },
	fetchInstitutions: () => { dispatch(fetchInstitutions()) },
	fetchInvestigators: () => { dispatch(fetchInvestigators()) }
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminManager);