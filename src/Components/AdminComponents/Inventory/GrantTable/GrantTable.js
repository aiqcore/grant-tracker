import React, { Component } from 'react';
import { connect } from "react-redux";
import { fetchAllGrants } from "../../../../state/actions/adminActions";
import { Table, Col } from 'antd';

const COLUMNS = [
    {
        title: 'Grant ID',
        dataIndex: 'id',
        key: "id"
    },
    {
        title: 'Title',
        dataIndex: 'grant_name',
        key: "title"
    },
    {
        title: 'Funding Source',
        dataIndex: 'funding_source',
        key: 'funding_source'
    },
    {
        title: 'Funder',
        dataIndex: "funder",
        key: "funder"
    },
    {
        title: 'Amount Requested',
        key: "amount_requested",
        render: (text) => {
            if (text.amount_requested) {
                return `$${text.amount_requested.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}`;
            }
        }
    },
    {
        title: "Budget",
        key: 'budget',
        render: (text) => {
            if (text.budget) {
                return `$${text.budget.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}`;
            }
        }
    },
    {
        title: 'Start Date',
        dataIndex: 'start_date',
        key: 'start_date'
    },
    {
        title: 'End Date',
        dataIndex: 'end_date',
        key: 'end_date'
    }
];

class GrantTable extends Component {
    componentWillMount() {
        this.props.fetchAllGrants();
    }

    render() {
        return (
            <Table size="small" columns={COLUMNS} rowKey='id'
                   loading={this.props.isFetchingAllGrants} dataSource={this.props.grants}
                   expandedRowRender={record => (
                    <div>
                        <Col sm={18} md={12}>
                            <h4 className="transaction-subhead">Prime Awardee</h4>
                            <p style={{ margin: 0 }}>{record['prime_awardee']}</p>
                        </Col>
                        <Col sm={6} md={12}>
                            <h4 className="transaction-subhead">Authorizer</h4>
                            <p>{record["authorizer_name"]}</p>
                        </Col>
                    </div>
                )}
            />
        )
    }
}

const mapStateToProps = state => ({
    ...state.admin
});

const mapDispatchToProps = dispatch => ({
    fetchAllGrants: () => { dispatch(fetchAllGrants()) }
});

export default connect(mapStateToProps, mapDispatchToProps)(GrantTable);