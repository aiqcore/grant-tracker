import React, { Component } from 'react';
import {connect} from "react-redux";
import { fetchAllExpenses } from "../../../../state/actions/adminActions";
import { Table } from 'antd';

const COLUMNS = [
    {
        title: 'Expense ID',
        dataIndex: 'id',
        key: "id"
    },
    {
        title: 'Category',
        dataIndex: 'category',
        key: "category"
    },
    {
        title: 'Paid To',
        key: 'paid_to',
        dataIndex: 'paid_to'
    },
    {
        title: 'Date Paid',
        dataIndex: "date_paid",
        key: "date_paid"
    },
    {
        title: "Amount",
        key: 'amount',
        render: (text) => {
            if (text.amount) {
                return `$${text.amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}`;
            }
        }
    },
];

class TransactionTable extends Component {
    componentWillMount() {
        this.props.fetchAllExpenses();
    }

    render() {
        return (
            <Table size="small" columns={COLUMNS} rowKey="id" loading={this.props.isFetchingAllExpenses} dataSource={this.props.expenses} />
        );
    }
}


const mapStateToProps = state => ({
    ...state.admin
});

const mapDispatchToProps = dispatch => ({
    fetchAllExpenses: () => { dispatch(fetchAllExpenses()) }
});

export default connect(mapStateToProps, mapDispatchToProps)(TransactionTable);