import React from 'react';
import { Tabs, Icon } from 'antd';

import Layout from '../../Shared/Layout/Layout';
import UserTable from './UserTable/UserTable';
import GrantTable from './GrantTable/GrantTable';
import TransactionTable from './TransactionTable/TransactionTable';

export default function Inventory() {
    return (
        <div className="admin-container">
            <Layout title={"Record Inventory"} description={"Use this panel to view all records currently in the system."} />
            <Tabs defaultActiveKey="users">
                <Tabs.TabPane tab={<span><Icon type="user" />Users</span>} key="users">
                    <UserTable />
                </Tabs.TabPane>
                <Tabs.TabPane tab={<span><Icon type="solution" />Grants</span>} key="grants">
                    <GrantTable />
                </Tabs.TabPane>
                <Tabs.TabPane tab={<span><Icon type="shopping-cart" />Transactions</span>} key="transactions">
                    <TransactionTable />
                </Tabs.TabPane>
            </Tabs>
        </div>
    )
}