import React, { Component } from 'react';
import { connect } from "react-redux";
import { fetchAllUsers } from "../../../../state/actions/adminActions";
import { Table } from 'antd';

const COLUMNS = [
    {
        title: 'Name',
        key: 'firstName',
        render: (text) => {
            let salutation = text.Salutation ? text.Salutation : '';
            return `${salutation} ${text["First Name"]} ${text["Last Name"]}`
        }
    },
    {
        title: 'Contact E-Mail',
        dataIndex: "Contact Email",
        key: "email"
    },
    {
        title: 'Contact Phone',
        key: "phone",
        render: (text) => {
            let s2 = (""+text["Contact Mobile Numbers"]).replace(/\D/g, '');
            let m = s2.match(/^(\d{3})(\d{3})(\d{4})$/);
            return (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        }
    }
];

class UserTable extends Component {
    componentWillMount() {
        this.props.fetchAllUsers();
    }

    render() {
        return (
            <Table size="small" columns={COLUMNS} rowKey="Contact Email" loading={this.props.isFetchingAllUsers} dataSource={this.props.users} />
        );
    }
}

const mapStateToProps = state => ({
    ...state.admin
});

const mapDispatchToProps = dispatch => ({
    fetchAllUsers: () => { dispatch(fetchAllUsers()) }
});

export default connect(mapStateToProps, mapDispatchToProps)(UserTable);