import React from 'react';
import renderer from 'react-test-renderer';
import UploadButton from './UploadButton';

describe('UploadButton', () => {
    it('renders correctly', () => {
        const tree = renderer.create(
            <UploadButton text="Grants" />
        ).toJSON();
        expect(tree).toMatchSnapshot();
    });
});