import React from 'react';
import Button from 'antd/lib/button';
import Icon from 'antd/lib/icon';
import Upload from 'antd/lib/upload';
import PropTypes from 'prop-types';

import { BASE_API_URL } from '../../../state/config';

export default function UploadButton(props) {
    let type = props.text.toLowerCase();
    let id = `upload-${type}`;
    let name = `${type}_file`;
    let action = `${BASE_API_URL}/${type}/bulk`;

    return (
        <Upload 
            id={id} 
            name={name}
            action={action}
            style={{ marginBottom: '15px' }}>
            <Button>
                <Icon type="upload"/>Upload {props.text}
            </Button>
        </Upload>
    )
};

UploadButton.propTypes = {
    text: PropTypes.oneOf(['Grants', 'Expenses']).isRequired
};