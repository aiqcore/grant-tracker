import React from 'react';
import Button from 'antd/lib/button';
import PropTypes from 'prop-types';

import './CreateButton.css';

export default function CreateButton({ showModal, text, iconType}) {
    let lowerText = text.toLowerCase();
    let id = `create-${lowerText}-button`;

    return (
        <div>
            <Button 
                id={id}
                className="create-button blue-bg" 
                type="primary" 
                shape="circle"
                onClick={showModal} 
                icon={iconType}/>
            <p className="button-caption">{text}</p>
        </div>
    )
}

CreateButton.propTypes = {
    showModal: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
    iconType: PropTypes.string.isRequired
}