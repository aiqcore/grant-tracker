import React from 'react';
import renderer from 'react-test-renderer';
import CreateButton from './CreateButton';

describe('CreateButton', () => {
    it('renders correctly', () => {
        const tree = renderer.create(
            <CreateButton text="Grants" iconType="solution" showModal={console.log} />
        ).toJSON();
        expect(tree).toMatchSnapshot();
    });
});