import React from 'react';
import PropTypes from 'prop-types';
import Button from 'antd/lib/button';

export default function CreateModalFooter({ isEdit, clearForm, submitForm, deleteRecord, form }) {
    return [
        <Button key="clear" type="ghost" onClick={() => clearForm(form)}>Clear Form</Button>,
        isEdit ? <Button key="delete" type="danger" onClick={deleteRecord}>Delete</Button> : null,
        <Button type="primary" key="register" onClick={submitForm}>{isEdit ? `Save` : `Register`}</Button>
    ]
}

CreateModalFooter.propTypes = {
    isEdit: PropTypes.bool.isRequired,
    clearForm: PropTypes.func.isRequired,
    deleteRecord: PropTypes.func.isRequired,
    submitForm: PropTypes.func.isRequired,
    form: PropTypes.any
};