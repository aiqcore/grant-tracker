import React from 'react';
import PropTypes from 'prop-types';

import CreateButton from '../CreateButton/CreateButton';
import GrantCreation from '../AdminCreationModal/GrantCreation';
import TransactionCreation from '../AdminCreationModal/TransactionCreation';
import UserCreation from '../AdminCreationModal/UserCreation';
import UploadButton from '../UploadButton/UploadButton';

export default function CreateButtonContainer(props) {
    let text = "";
    let iconType = "";
    let modal = null;
    let uploadButton = null;
    let id = `${props.type}-modal`;

    switch (props.type) {
        case "user":
            text = "Users";
            iconType = "user";
            modal = <UserCreation id={id} visible={props.modalVisible} handleCancel={props.handleCancel} />;
            break;
        case "grant":
            text = "Grants";
            iconType = "solution";
            modal = <GrantCreation id={id} visible={props.modalVisible} handleCancel={props.handleCancel} />;
            uploadButton = <UploadButton text="Grants"/>;
            break;
        case "transaction":
            text = "Expenses";
            iconType = "shopping-cart";
            modal = <TransactionCreation id={id} visible={props.modalVisible} handleCancel={props.handleCancel} />;
            uploadButton = <UploadButton text="Expenses" />;
            break;
        default:
            break;
    }
    
    return (
        <div>
            <CreateButton
                showModal={props.showModal}
                text={text}
                iconType={iconType} />
            {modal}
            {uploadButton}
        </div>
    );
}

CreateButtonContainer.propTypes = {
    type: PropTypes.oneOf(['grant', 'user', 'transaction']).isRequired,
    showModal: PropTypes.func.isRequired,
    modalVisible: PropTypes.bool.isRequired,
    handleCancel: PropTypes.func.isRequired
}