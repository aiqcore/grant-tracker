export function validatePassword(password) {
    if (password.length < 8) {
        return {
            validateStatus: 'error',
            errorMsg: 'Password must be at least 8 characters.'
        }
    }
    if (!/\d/.test(password)) {
        return {
            validateStatus: 'error',
            errorMsg: 'Password must contain an integer.'
        }
    }

    return {
        validateStatus: 'success',
        errorMsg: null
    }
}

export function validateZip(zip) {
    if (/[a-zA-Z]/.test(zip)) {
        return {
            validateStatus: 'error',
            errorMsg: 'Zip cannot contain letters.'
        }
    }

    if (zip.length !== 5) {
        return {
            validateStatus: 'error',
            errorMsg: 'Zip code must be 5 numbers.'
        }
    }

    return {
        validateStatus: 'success',
        errorMsg: null
    }
}