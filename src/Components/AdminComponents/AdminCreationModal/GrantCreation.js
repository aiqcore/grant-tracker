import React from "react";
import { Form, Select, DatePicker, Input, Spin, InputNumber, Modal, AutoComplete, Divider } from 'antd';
import { connect } from "react-redux";
import { registerGrant, Actions, fetchGrant, updateGrant, deleteGrant, fetchAllGrants } from "../../../state/actions/adminActions";
import { Actions as ModalActions } from '../../../state/actions/modalActions';
import { dateFormat } from '../../../state/config';
import moment from 'moment';

import CreateModalFooter from '../CreateModalFooter/CreateModalFooter';

const { fetchUser } = Actions;

const GrantCreation = Form.create()(
    class extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
            	value: [],
                principalValue: {
					key: '',
					label: ''
				},
				coInvestigatorValue: [],
				subawardValue: [],
                grantQuery: '',
	            grant: {},
	            edit: false
            };
		}
		
		componentWillReceiveProps(nextProps) {
			if (nextProps.selectedGrantDetails) {
				this.populateFields(nextProps.selectedGrantDetails);
			}
		}

        /**
		 * Formats the grant string to be displayed in the autocomplete dropdown.
         * @param grant Grant object
         * @returns {string} Output string for display
         */
        formatGrantString(grant) {
        	return `${grant['grant_name']} (ID: ${grant['id']})`;
		}

        /**
		 * Update query value in grant autocomplete component.
         * @param value String value currently entered in the autocomplete box.
         */
	    handleGrantQuery(value) {
		    this.setState({ grantQuery: String(value) })
	    }

        /**
		 * When a grant is clicked from the autocomplete dropdown, fetch the details.
         * @param value Selected grant string.
         */
	    handleGrantSelect(value) {
			let grant = this.props.grants.find(grant => this.formatGrantString(grant) === value);
			this.props.fetchGrant(grant['id']);

			this.setState({
				grantQuery: value
			});
		}
		
		/**
		 * Populate the form fields from the grant object currently selected from the dropdown.
		 */
		populateFields = (grant) => {	
			// Do not populate if the grant hasn't loaded yet or if has already been loaded.
			if (grant === null || grant === this.state.grant) {
				return;
			}

			// Populate the non-date fields in the UI.
			this.props.form.setFieldsValue({
			    "grant_name": grant["grant_name"],
			    "log_number": grant["log_number"],
			    "proposal_title": grant["proposal_title"],
				"funding_source": grant["funding_source"],
				"primary_organization": grant["primary_organization"],
			    "funder": grant["funder"],
				"budget": grant["budget"]
		    });
			
			// Parse dates into format applicable for the datepicker component.
			const DATE_FIELDS = ['start_date', 'end_date', 'submission_date'];
			

		    for (let field of DATE_FIELDS) {
		    	if (grant[field] !== null && grant[field] !== '0000-00-00') {
		    		this.props.form.setFieldsValue({
						[field]: moment(grant[field], dateFormat)
					});
				}
            }
			
			// Parse the coinvestigator array into the format applicable for the multi-select.
			let parsedCoinvestigators = [];
			if (grant['coinvestigators']) {
				for (let investigator of grant['coinvestigators']) {
					parsedCoinvestigators.push({
						key: investigator['investigator_id'],
						label: investigator['investigator_name']
					});
				}
			}

			let parsedSubaward = [];
			if (grant['subaward_organizations']) {
				for (let organization of grant['subaward_organizations']) {
					parsedSubaward.push({
						key: organization,
						label: organization
					});
				}
			}

			// Parse the principal investigator for the applicable format.
			let principalKey = '';
			let principalValue = '';
			if (grant['principal_investigator']) {
				principalKey = grant['principal_investigator']['investigator_id'];
				principalValue = grant['principal_investigator']['investigator_name'];
			}
			
		    this.setState({
			    principalValue: { 
					key: principalKey, 
					label: principalValue,
				},
				coInvestigatorValue: parsedCoinvestigators,
				subawardValue: parsedSubaward,
			    edit: true,
			    grant
		    });
		}

	    handlePrincipalChange = (principalValue) => {
		    this.setState({
			    principalValue: {...principalValue}
		    });
	    };

	    handleCoInvestigatorChange = (coInvestigatorValue) => {
		    this.setState({
			    coInvestigatorValue
		    });
		};
		
		handleSubawardChange = (subawardValue) => {
			this.setState({
				subawardValue
			});
		}

        onDelete = () => {
            this.props.deleteGrant(this.state.grant['id']);
            this.props.form.resetFields();
			this.props.closeModal();
			
			this.setState({
                grantQuery: ''
            });
        };

        emitEmpty = () => {
	    	this.setState({
			    edit: false,
			    principalValue: '',
			    coInvestigatorValue: []
	    	});
	    	this.props.form.resetFields();
	    	this.grantSelectInput.value = '';
	    };

        onSubmit = (e) => {
            e.preventDefault();
            this.props.form.validateFieldsAndScroll((err, values) => {
                if (!err) {
					values["submission_date"] = values["submission_date"] ? values["submission_date"].toDate() : null;
					let principal_investigator = parseInt(this.state.principalValue['key'], 10);
					let coinvestigatorList = [];
					for (let investigator of this.state.coInvestigatorValue) {
						coinvestigatorList.push(parseInt(investigator['key'], 10));
					}

					let subawardList = [];
					for (let organization of this.state.subawardValue) {
						subawardList.push(organization['key']);
					}

	                if(this.state.edit) {
		                this.props.updateGrant({
			                ...values,
			                'coinvestigators': coinvestigatorList,
							'principal_investigator': principal_investigator,
							'subaward_organizations': subawardList,
			                id: this.state.grant['id']
		                })
	                } else {
		                this.props.registerGrant(
			                {
				                ...values,
				                'coinvestigators': coinvestigatorList,
								'principal_investigator': principal_investigator,
								'subaward_organizations': subawardList,
			                },
			                this.props.user);
	                }
                	this.setState({
		                value: [],
		                principalValue: {
							key: '',
							label: ''
						},
						coInvestigatorValue: [],
						primaryOrganizationValue: {
							key: '',
							label: ''
						},
						subawardValue: [],
						edit: false,
						grantQuery: ''
	                });
                    this.props.handleCancel();
                    this.props.form.resetFields();
                }
            });
		};
		
		onClearForm = (form) => {
			form.resetFields();

			this.setState({
				value: [],
                principalValue: {
					key: '',
					label: ''
				},
				coInvestigatorValue: [],
				subawardValue: [],
                grantQuery: '',
	            grant: {},
	            edit: false
			});
		}

        render() {
            const { form } = this.props;
            const formItemLayout = {
                labelCol: {
                    xs: { span: 24 },
                    sm: { span: 8 },
                },
                wrapperCol: {
                    xs: { span: 24 },
                    sm: { span: 16 },
                },
            };

            const { getFieldDecorator } = form;

	        let grants = [];

	        for (let grant of this.props.grants) {
	        	let query = this.state.grantQuery.toLowerCase();
		        if (grant["grant_name"].toLowerCase().startsWith(query)) {
			        grants.push(this.formatGrantString(grant));
		        }
	        }

            return (
	            <Modal
					title="Grant Management"
					maskClosable={false}
		            visible={this.props.visible}
					onCancel={this.props.closeModal}
					footer={<CreateModalFooter 
								clearForm={this.onClearForm} 
								form={form} 
								submitForm={this.onSubmit} 
								deleteRecord={this.onDelete}
								isEdit={this.state.edit} />}
	            >
	                <Form onSubmit={this.onSubmit}>
		                <p className="modal-description">Edit a grant by selecting them from the dropdown menu or create a new one by filling out the form
			                below.</p>
		                <p className="autocomplete-label">Select a grant to edit:</p>
		                <AutoComplete className="edit-autocomplete" size="large" placeholder="Search for a grant by name..."
									  dataSource={grants}
									  allowClear
									  value={this.state.grantQuery}
		                              onSearch={(value) => this.handleGrantQuery(value)}
		                              onSelect={(value) => this.handleGrantSelect(value)}
		                >
			                <Input onClick={this.props.fetchAllGrants}/>
		                </AutoComplete>
		                <Divider />
	                    <Form.Item {...formItemLayout} label={"Grant Number"}>
	                        {getFieldDecorator('grant_name', {
	                            rules: [{
	                                required: true, message: 'Please input a number identifier for the grant!'
	                            }]
	                        })(
	                            <Input name="grantName"/>
	                        )}
	                    </Form.Item>
                        <Form.Item {...formItemLayout} label={"Submission Date"}>
                            {getFieldDecorator('submission_date', {
                                rules: [{
                                    required: true, message: 'Please enter a submission date!'
                                }]
                            })(
                                <DatePicker/>
                            )}
                        </Form.Item>
	                    <Form.Item {...formItemLayout} label="eBRAP Log Number">
	                        { getFieldDecorator('log_number', { 
								rules: [{ 
									message: 'Please a proper Log number' 
								}]
							})(<Input name={'logNumber'}/>) 
							}
	                    </Form.Item>
	                    <Form.Item {...formItemLayout} label={"Proposal Title"}>
	                        { getFieldDecorator('proposal_title', {
								rules: [{ 
									required: true, 
									message: 'Please input a Proposal Title for the grant!' 
								}]
							})(<Input name="proposalTitle"/>)
							}
	                    </Form.Item>
	                    <Form.Item {...formItemLayout} label={"Start Date"}>
	                        { getFieldDecorator('start_date', {
	                            rules: [{
	                                required: true, message: 'Please enter a start date'
	                            }]
							})(<DatePicker/>)
							}
	                    </Form.Item>
	                    <Form.Item {...formItemLayout} label={"End Date"}>
	                        {getFieldDecorator('end_date', {
	                            rules: [{
	                                required: true, message: 'Please enter an end date'
	                            }]
	                        })(
	                            <DatePicker/>
	                        )}
	                    </Form.Item>
	                    <Form.Item
	                        label="Funding Source"
	                        labelCol={{ span: 8 }}
	                        wrapperCol={{ span: 16 }}
	                    >
	                        {getFieldDecorator('funding_source', {
	                            rules: [{  message: 'Please select a valid funding source' }],
	                        })(
	                            <Select placeholder="Federal, Private, or Industry" onChange={this.handleSelectChange}>
	                                <Select.Option value="federal">Federal</Select.Option>
	                                <Select.Option value="private">Private</Select.Option>
	                                <Select.Option value="industry">Industry</Select.Option>
	                            </Select>
	                        )}
	                    </Form.Item>
	                    <Form.Item {...formItemLayout} label={"Funder"}>
	                        {getFieldDecorator('funder', {
	                            rules: [{
	                                message: 'Please input a funder for the grant!'
	                            }]
	                        })(
	                            <Input name="funder"/>
	                        )}
	                    </Form.Item>
	                    <Form.Item {...formItemLayout} label="Principal Investigator">
	                        <Select
								value={this.state.principalValue}
								labelInValue
	                            placeholder="Apply a Principal Investigator to this grant."
	                            notFoundContent={this.state.fetching ? <Spin size="small" /> : null}
	                            filterOption={false}
	                            onChange={this.handlePrincipalChange}
	                            style={{ width: '100%' }}
	                        >
		                        {
			                        this.props.investigators ? this.props.investigators.map(investigator => (
											<Select.Option key={investigator['Client ID']}>
												{ `${investigator['First Name']} ${investigator['Last Name']}` }
											</Select.Option>
				                        )) : null
		                        }
	                        </Select>
	                    </Form.Item>
	                    <Form.Item {...formItemLayout} label="Co-Investigators">
	                        <Select
	                            mode="multiple"
								value={this.state.coInvestigatorValue}
								labelInValue
	                            placeholder="Apply Co-Investigating users to this grant..."
	                            notFoundContent={this.state.fetching ? <Spin size="small" /> : null}
	                            filterOption={false}
	                            onChange={this.handleCoInvestigatorChange}
	                            style={{ width: '100%' }}
	                        >
		                        {
			                        this.props.investigators ? this.props.investigators.map(investigator => (
				                        <Select.Option key={parseInt(investigator["Client ID"], 10)}>
					                        { `${investigator['First Name']} ${investigator['Last Name']}` }
										</Select.Option>
				                        )
		                            ) : null
		                        }
	                        </Select>
	                    </Form.Item>
	                    <Form.Item {...formItemLayout} label={"Prime Organization"}>
	                        {getFieldDecorator('primary_organization', {
	                            rules: [{
	                                message: 'Please input a Prime Organization'
	                            }]
	                        })(
	                            <Select placeholder="Please select an institution.">
                                    { this.props.institutions.map(institution => (
                                        <Select.Option key={institution.institution_name}>
                                            {institution.institution_name}
                                        </Select.Option>
                                    ))}
                                </Select>
	                        )}
	                    </Form.Item>
	                    <Form.Item {...formItemLayout} label="Subaward Organizations">
	                        <Select
	                            mode="multiple"
								value={this.state.subawardValue}
								labelInValue
	                            placeholder="Apply Subaward Organizations to this grant..."
	                            notFoundContent={this.state.fetching ? <Spin size="small" /> : null}
	                            filterOption={false}
	                            onChange={this.handleSubawardChange}
	                            style={{ width: '100%' }}
	                        >
		                        {
			                        this.props.institutions ? this.props.institutions.map(institution => (
				                        <Select.Option key={institution.institution_name}>
					                        { institution.institution_name }
										</Select.Option>
				                        )
		                            ) : null
		                        }
	                        </Select>
	                    </Form.Item>
	                    <Form.Item
	                        {...formItemLayout}
	                        label="Amount"
	                    >
	                        {getFieldDecorator('budget', {
	                            rules: [ {
	                                required: true, message: 'Please enter a budget amount for the grant.',
	                            }],
	                        })(
	                            <InputNumber
	                                initialValue={0}
	                                formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
	                                parser={value => value.replace(/\$\s?|(,*)/g, '')}
	                            />
	                        )}
	                    </Form.Item>
	                </Form>
	            </Modal>
            );
        }
    }
);

const mapStateToProps = state => {
	return {
		...state.admin,
		...state.grant,
		user: state.login.user
	}
};

const mapDispatchToProps = dispatch => (
	{
        registerGrant: (grant, user) =>{ dispatch(registerGrant(grant, user)) },
        fetchUser: (id) => {dispatch(fetchUser(id))},
		fetchAllGrants: () => { dispatch(fetchAllGrants()) },
		fetchGrant: (grantID) => { dispatch(fetchGrant(grantID)) },
        closeModal: () => { dispatch(ModalActions.closeModals()) },
		updateGrant: grant => { dispatch(updateGrant(grant)) },
		deleteGrant: grantID => { dispatch(deleteGrant(grantID)) }
	}
);

export default connect(mapStateToProps, mapDispatchToProps)(GrantCreation);