import React from "react";
import {Form, Select, DatePicker, Input, AutoComplete, Divider, InputNumber, Modal} from 'antd';
import {connect} from "react-redux";
import moment from 'moment';
import { fetchAllGrants, createExpense, fetchAllUsers, fetchAllExpenses, deleteExpense, updateExpense } from "../../../state/actions/adminActions";
import { EXPENSE_TYPES } from "../../../state/config";

import CreateModalFooter from '../CreateModalFooter/CreateModalFooter';

const TransactionCreation = Form.create()(
    class extends React.Component {
        state = {
            query: '',
            itemSelected: null
        };

        formatExpenseString(expense) {
            return `${expense.id} | ${expense.paid_to} ${expense.date_paid} (Grant #${expense.grant})`;
        }

        onSubmit = (e) => {
            e.preventDefault();
            this.props.form.validateFieldsAndScroll((err, values) => {
                if (!err) {
                    values['grant'] = parseInt(values['grant'], 10);
                    values['authorizer'] = parseInt(values['authorizer'], 10);
                    values['date_paid'] = moment(values['date_paid']).format('YYYY-MM-DD');
                    if (this.state.itemSelected) {
                        this.props.updateExpense(this.state.itemSelected, values);
                    } else {
                        this.props.createExpense(values);
                    }
                    this.props.handleCancel();
                    this.props.form.resetFields();
                    this.setState({
                        query: ''
                    });
                }
            });
        };

        onDelete = () => {
            this.props.deleteExpense(this.state.itemSelected);
            this.props.form.resetFields();
            this.props.handleCancel();
            this.setState({
                query: ''
            });
        };

        handleQuery = (query) => {
            this.setState({ query });
        };

        handleSelect = (expenseID) => {
            let expense = this.props.expenses.find( expense => this.formatExpenseString(expense) === expenseID);
            this.props.form.setFieldsValue({
                grant: expense['grant'],
                amount: parseFloat(expense['amount'].toFixed(2)),
                category: expense['category'],
                authorizer: expense['authorizer'],
                misc_note: expense['misc_note'],
                paid_to: expense['paid_to'],
            });

            if (expense['date_paid'] !== '0000-00-00') {
                this.props.form.setFieldsValue({
                    date_paid: moment(expense['date_paid'])
                });
            }

            this.setState({ 
                itemSelected: expense.id,
                query: expenseID
            });
        };

        filterAutocomplete = () => {
            let expenses = [];

            for (let expense of this.props.expenses) {
                let query = this.state.query.toLowerCase();
                if (expense['paid_to'].toLowerCase().startsWith(query)) {
                    expenses.push(this.formatExpenseString(expense));
                }
            }
            return expenses;
        };

        onClear = (form) => {
            form.resetFields();

            this.setState({
                query: '',
                itemSelected: null
            });
        }

        render() {
            const { form } = this.props;
            const formItemLayout = {
                labelCol: {
                    xs: { span: 24 },
                    sm: { span: 8 },
                },
                wrapperCol: {
                    xs: { span: 24 },
                    sm: { span: 16 },
                },
            };
            const { getFieldDecorator } = form;

            let expenses = this.filterAutocomplete();

            return (
                <Modal
                    title="Transaction Management"
                    visible={this.props.visible}
                    maskClosable={false}
                    onCancel={this.props.handleCancel}
                    footer={<CreateModalFooter 
                                isEdit={this.state.itemSelected !== null}
                                form={form}
                                deleteRecord={this.onDelete}
                                submitForm={this.onSubmit} 
                                clearForm={this.onClear} />}
                >
                    <Form onSubmit={this.onSubmit}>
                        <p className="modal-description">Edit an expense by selecting them from the dropdown menu or create a new one by filling out the form
                            below.</p>
                        <p className="autocomplete-label">Select an expense to edit:</p>
                        <AutoComplete className="edit-autocomplete" size="large" placeholder="Search for expense..."
                                      dataSource={expenses}
                                      onSearch={(value) => this.handleQuery(value)}
                                      allowClear
                                      value={this.state.query}
                                      onSelect={(value) => this.handleSelect(value)}
                        >
                            <Input onClick={this.props.fetchAllExpenses}/>
                        </AutoComplete>
                        <Divider />
                        <Form.Item
                            {...formItemLayout}
                            label="Grant"
                        >
                            {getFieldDecorator('grant', {
                                rules: [{
                                    required: true, message: 'Please choose a grant to apply the expense!',
                                }],
                            })(
                                <Select onFocus={this.props.fetchAllGrants}>
                                    { this.props.grants.map(grant => <Select.Option key={grant.id}>{`${grant.grant_name} (#${grant.id})`}</Select.Option>) }
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Amount"
                        >
                            {getFieldDecorator('amount', {
                                rules: [{
                                    required: true, message: 'Please input an amount!',
                                }],
                            })(
                                <InputNumber formatter={value => `$${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                             parser={value => value.replace(/\$\s?|(,*)/g, '')}/>

                            )}
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Category"
                        >
                            {getFieldDecorator('category', {
                                rules: [{
                                    required: true, message: 'Please choose a category for the expense!',
                                }],
                            })(
                                <Select>
                                    { EXPENSE_TYPES.map(type => <Select.Option key={type}>{type}</Select.Option>) }
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Paid To:"
                        >
                            {getFieldDecorator('paid_to', {
                                rules: [{
                                    required: true, message: 'Please input your e-mail!',
                                }],
                            })(
                                <Input />
                            )}
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Notes"
                        >
                            {getFieldDecorator('misc_note', {
                            })(
                                <Input.TextArea rows={3} />
                            )}
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Date Paid"
                        >
                            {getFieldDecorator('date_paid')(
                                <DatePicker />
                            )}
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Authorizer"
                        >
                            {getFieldDecorator('authorizer', {
                                rules: [{
                                    required: true, message: 'Please choose a user authorizing the expense!',
                                }],
                            })(
                                <Select onFocus={this.props.fetchAllUsers}>
                                    { this.props.users.map(user => <Select.Option key={user["Client ID"]}>{`${user['First Name']} ${user['Last Name']} (#${user['Client ID']})`}</Select.Option>) }
                                </Select>
                            )}
                        </Form.Item>
                    </Form>
                </Modal>
            );
        }
    }
);

const mapStateToProps = state => {
    return {
        ...state.admin
    }
};

const mapDispatchToProps = dispatch => (
    {
        fetchAllGrants: () => { dispatch(fetchAllGrants()) },
        createExpense: (expense) => { dispatch(createExpense(expense)) },
        fetchAllUsers: () => { dispatch(fetchAllUsers()) },
        fetchAllExpenses: () => { dispatch(fetchAllExpenses()) },
        deleteExpense: (expenseID) => { dispatch(deleteExpense(expenseID)) },
        updateExpense: (expenseID, expense) => { dispatch(updateExpense(expenseID, expense)) }
    }
);

export default connect(mapStateToProps, mapDispatchToProps)(TransactionCreation);