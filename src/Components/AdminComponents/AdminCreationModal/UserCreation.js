import React from "react";
import { Form, Input, Radio, Select, Modal, DatePicker, Divider, AutoComplete } from 'antd';
import { connect } from "react-redux";
import moment from 'moment';
import { registerUser, fetchAllUsers, deleteUser, updateUser } from "../../../state/actions/adminActions";
import { validatePassword, validateZip } from "./validations";
import { cleanObject } from '../../helpers';
import { Actions } from '../../../state/actions/modalActions';

import CreateModalFooter from '../CreateModalFooter/CreateModalFooter';

import './Modals.css';

const UserCreation = Form.create()(
    class extends React.Component {
        state = {
                password: {
                    value: ''
                },
                zip: {
                    value: ''
                },
                userQuery: '',
                itemSelected: null
        };

        handleUserQuery(value) {
            this.setState({ userQuery: String(value) })
        }

        handleUserSelect(value) {
            let user = this.props.users.find( user => {
                return this.formatUserString(user) === value });

            this.props.form.setFieldsValue({
                "salutation": user["Salutation"],
                "email": user["Contact Email"],
                "firstName": user["First Name"],
                "lastName": user["Last Name"],
                "jobTitle": user["Job Title"],
                "mobile-number": user["Contact Mobile Numbers"],
                "gender": user["Gender"],
                "referral-source": user["Referral Source"],
                "research-facility-id": user["Research Facility ID"],
                "notes": user["Notes"],
                "address": user["Address"],
                "user-type": user["admin"]
            });

            if (user['Birth Date'] !== null && user['Birth Date'] !== '0000-00-00') {
                this.props.form.setFieldsValue({
                    'birth-date': moment(user['Birth Date'])
                });
            }

            this.setState({
                zip: { value: user["Zip Code"]},
                itemSelected: user["Client ID"],
                userQuery: value
            });
        }

        formatUserString(user) {
            let salutation = user.Salutation ? user.Salutation : '';
        	return `${salutation} ${user['First Name']} ${user['Last Name']} (${user['Contact Email']})`;
		}

        onDelete = () => {
            this.props.deleteUser(this.state.itemSelected);
            this.props.form.resetFields();
            this.props.closeModal();

            this.setState({
                userQuery: ''
            });
        };

        handlePasswordChange = (e) => {
            let value = e.target.value;
            this.setState({
                password: {
                    ...validatePassword(value),
                    value
                }
            })
        };

        handleZipChange = (e) => {
            let value = e.target.value;
            this.setState({
                zip: {
                    ...validateZip(value),
                    value
                }
            })
        };

        onSubmit = () => {
            this.props.form.validateFieldsAndScroll((err, values) => {
                if (!err) {
                    const isAdmin = values["user-type"];
                    values["birth-date"] = values["birth-date"] ? values["birth-date"].toDate() : null;
                    values["password"] = this.state.password.value;
                    values["zip"] = this.state.zip.value;
                    for (let key of Object.keys(values)) {
                        if (!values[key]) {
                            values[key] = '';
                        }
                    }

                    if (this.state.itemSelected) {
                        this.props.updateUser(this.state.itemSelected, values);
                    } else {
                        values = cleanObject(values);
                        this.props.registerUser(values, isAdmin);
                    }

                    this.props.closeModal();
                    this.props.form.resetFields();

                    this.setState({
                        userQuery: '',
                        password: { value: '' },
                        itemSelected: null
                    });
                }
            });
        };

        filterAutocomplete = () => {
            let users = [];

            for (let user of this.props.users) {
                let query = this.state.userQuery.toLowerCase();
                let fullName = (user["First Name"] + " " + user["Last Name"]).toLowerCase();
                if (user["Contact Email"].toLowerCase().startsWith(query) ||
                    fullName.startsWith(query) ||
                    user["First Name"].toLowerCase().startsWith(query) ||
                    user["Last Name"].toLowerCase().startsWith(query)) {
                        users.push(this.formatUserString(user));
                }
            }
            
            return users;
        };

        onClear = (form) => {
            form.resetFields();

            this.setState({
                password: {
                    value: ''
                },
                zip: {
                    value: ''
                },
                userQuery: '',
                itemSelected: null
            });
        }

        render() {
            const { form } = this.props;
            const formItemLayout = {
                labelCol: {
                    xs: { span: 24 },
                    sm: { span: 8 },
                },
                wrapperCol: {
                    xs: { span: 24 },
                    sm: { span: 16 },
                },
            };
            const { getFieldDecorator } = form;

            const password = this.state.password;
            const passwordTips = "Password must be 8 characters and contain an integer.";

            const zip = this.state.zip;

            let users = this.filterAutocomplete();

            return (
                <Modal
                    closable={true}
                    title="User Management"
                    maskClosable={false}
                    visible={this.props.visible}
                    onCancel={this.props.closeModal}
                    footer={<CreateModalFooter
                                isEdit={this.state.itemSelected !== null} 
                                deleteRecord={this.onDelete} 
                                submitForm={this.onSubmit} 
                                form={form}
                                clearForm={this.onClear} />}
                >
                    <Form>
                        <p className="modal-description">Edit a user by selecting them from the dropdown menu or create a new one by filling out the form
                            below.</p>
                        <p className="autocomplete-label">Select a user to edit:</p>
                        <AutoComplete className="edit-autocomplete" size="large" placeholder="Enter name or e-mail..."
                                      dataSource={users}
                                      onSearch={(value) => this.handleUserQuery(value)}
                                      allowClear
                                      value={this.state.userQuery}
                                      onSelect={(value) => this.handleUserSelect(value)}
                        >
                            <Input onClick={this.props.fetchAllUsers}/>
                        </AutoComplete>
                        <Divider />
                        <Form.Item
                            {...formItemLayout}
                            label="E-mail"
                        >
                            {getFieldDecorator('email', {
                                rules: [{
                                    type: 'email', message: 'The input is not valid E-mail!',
                                }, {
                                    required: true, message: 'Please input your e-mail!',
                                }],
                            })(
                                <Input />
                            )}
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Password"
                            id="password"
                            validateStatus={password.validateStatus}
                            help={password.errorMsg || passwordTips}
                        >
                            <Input disabled={this.state.itemSelected !== null} type={"password"} required value={password.value} onChange={this.handlePasswordChange}/>
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Salutation"
                            hasFeedback
                        >
                            {getFieldDecorator('salutation')(
                                <Select placeholder="Please select a title">
                                    { this.props.salutations.map(salutation => (
                                        <Select.Option key={salutation.salutation_name}>
                                            {salutation.salutation_name}
                                        </Select.Option>
                                    ))}
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout} label={"First Name"}>
                            {getFieldDecorator('firstName', {
                                rules: [{
                                    required: true, message: 'Please input your first name!'
                                }]
                            })(
                                <Input />
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout} label={"Last Name"}>
                            {getFieldDecorator('lastName', {
                                rules: [{
                                    required: true, message: 'Please input your last name!'
                                }]
                            })(
                                <Input />
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout} label={"Job Title"}>
                            {getFieldDecorator('jobTitle')(
                                <Input />
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout} label={"Mobile Number"}>
                            {getFieldDecorator('mobile-number')(
                                <Input />
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout} label={"Referral Source"}>
                            {getFieldDecorator('referral-source')(
                                <Input />
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout} label={"Research Facility ID"}>
                            {getFieldDecorator('research-facility-id')(
                                <Input />
                            )}
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Birth Date"
                        >
                            {getFieldDecorator('birth-date')(
                                <DatePicker />
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout} label={"Address"}>
                            {getFieldDecorator('address')(
                                <Input />
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout}
                                   label={"Zip Code"}
                                   validateStatus={zip.validateStatus}
                                   help={zip.errorMsg || null}
                        >
                            <Input value={zip.value} onChange={this.handleZipChange} />
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Gender"
                        >
                            {getFieldDecorator('gender', {
                                rules: [{
                                    required: true, message: 'Please choose a gender!',
                                }],
                            })(
                                <Radio.Group>
                                    <Radio.Button id="male-option" value="M">Male</Radio.Button>
                                    <Radio.Button id="female-option" value="F">Female</Radio.Button>
                                </Radio.Group>
                            )}
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="User Type"
                        >
                            {getFieldDecorator('user-type', {
                                rules: [{
                                    required: true, message: 'Please select a user type!'
                                }],
                                initialValue: 0
                            })(
                                <Radio.Group>
                                    <Radio.Button id="admin-option" value={1}>Admin</Radio.Button>
                                    <Radio.Button id="user-option" value={0}>User</Radio.Button>
                                </Radio.Group>
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout} label={"Notes"}>
                            {getFieldDecorator('notes')(
                                <Input.TextArea rows={2}/>
                            )}
                        </Form.Item>
                    </Form>
                </Modal>
            );
        }
    }
);


const mapStateToProps = state => {
	return {
		...state.admin,
		user: state.login.user,
	}
};

const mapDispatchToProps = dispatch => (
	{
		registerUser: (user, isAdmin) => { dispatch(registerUser(user, isAdmin)) },
        fetchAllUsers: () => { dispatch(fetchAllUsers()) },
        closeModal: () => { dispatch(Actions.closeModals()) },
        deleteUser: (userID) => { dispatch(deleteUser(userID)) },
        updateUser: (userID, user) => { dispatch(updateUser(userID, user)) }
	}
);

export default connect(mapStateToProps, mapDispatchToProps)(UserCreation);