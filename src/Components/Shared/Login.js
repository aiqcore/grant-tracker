import React, { Component } from 'react';
import { Grid, Form, Segment, Image } from 'semantic-ui-react';
import { Spin, Icon } from 'antd';
import { Actions, login } from '../../state/actions/loginActions';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './Login.css';

const { handleEmailChange, handlePwChange } = Actions;

class Login extends Component {
	render() {
	    return (
			<div className='login-form'>
				<Grid
					textAlign='center'
					style={{ height: '100%' }}
					verticalAlign='top'
					className="login-box"
				>
					<Grid.Column style={{ maxWidth: 450 }}>
						<Form size='large'>
							<Segment >
								<Image src='images/metis-logo.png'/>
								<h2>Login to your account</h2>
								<Form.Input
									fluid
									icon='user'
									iconPosition='left'
									placeholder='Username'
									name="username"
									onChange={(e) => this.props.handleEmailChange(e.target.value)}
								/>
								<Form.Input
									fluid
									icon='lock'
									iconPosition='left'
									placeholder='Password'
									type='password'
									name="password"
									onChange={(e) => this.props.handlePwChange(e.target.value)}
								/>
								<Form.Button 
									fluid 
									size='large' 
									disabled={!this.props.email || !this.props.pw}
									color='blue' 
									style={{ height: '50px' }}
									onClick={() => this.props.login(this.props.email, this.props.pw)}>
										{ !this.props.isLoggingIn ? 
										"Submit" : 
										<Spin size="small" indicator={
											<Icon type="loading" style={{ fontSize: 24, color: 'white' }} spin />} /> 
										}
								</Form.Button>
								<Link to={'/forgotpassword'}><small>Forgot Password?</small></Link>
							</Segment>
						</Form>
					</Grid.Column>
				</Grid>
			</div>
        )
    }
}

const mapStateToProps = state => {
	return {...state.login}
}

const mapDispatchToProps = dispatch => (
	{
		login: (username, password) => { dispatch(login(username, password)) },
		handleEmailChange: (email) => { dispatch(handleEmailChange(email)) },
		handlePwChange: (pw) => { dispatch(handlePwChange(pw)) }
	}
);

export default connect(mapStateToProps, mapDispatchToProps)(Login);