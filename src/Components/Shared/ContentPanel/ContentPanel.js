import React from 'react';
import PropTypes from 'prop-types';
import { Layout } from 'antd';

import AppMenu from '../AppMenu/AppMenu';
import GrantView from '../../UserComponents/GrantView/GrantView';
import AdminManager from '../../AdminComponents/AdminManager/AdminManager';
import Inventory from '../../AdminComponents/Inventory/Inventory';
import LoggedInHeader from '../LoggedInHeader';

import './ContentPanel.css';

const { Content, Header } = Layout;

export default function ContentPanel({ panelType, activeTab, menuOpen, navigateTab }) {
    let activeComponent = null;   
    
    switch (activeTab) {
        case 'overview':
            activeComponent = <GrantView />;
            break;
        case 'manage':
            activeComponent = <AdminManager />;
            break;
        case 'inventory':
            activeComponent = <Inventory />;
            break;
        default:
            if (panelType === 'admin') {
                activeComponent = <AdminManager />;
            } else {
                activeComponent = <GrantView />;
            }
    }

    return (
        <Layout className="user-panel">
            <AppMenu 
                menuOpen={menuOpen} 
                navigateTab={navigateTab} 
                menuType={panelType} />
            <Content>
                <Layout>
                    <Header>
                        <LoggedInHeader/>
                    </Header>
                    { activeComponent }
                </Layout>
            </Content>
        </Layout>
    );
}

ContentPanel.propTypes = {
    panelType: PropTypes.oneOf(['user', 'admin']).isRequired,
    activeTab: PropTypes.oneOf(['overview', 'manage', 'inventory']),
    menuOpen: PropTypes.bool.isRequired,
    navigateTab: PropTypes.func.isRequired
};