import React from 'react';
import { Grid, Form, Segment, Image } from 'semantic-ui-react';
import { confirmPassword } from '../../state/actions/loginActions';
import url from 'url';
import { NotificationManager } from 'react-notifications';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { NOTIFICATION_TIMEOUT, NOTIFICATION_ERROR } from "../../state/config";
import { Redirect } from 'react-router';

class ConfirmPassword extends React.Component {
	state = {
		pass1: '',
		pass2: ''
	}

	handlePass1Change = (e) => {
		this.setState({ pass1: e.target.value });
	};

	handlePass2Change = (e) => {
		this.setState({pass2: e.target.value});
	};

	onSubmit() {
		if(this.state.pass1 !== this.state.pass2) {
			NotificationManager.error('Passwords do not match. Please try again.', NOTIFICATION_ERROR, NOTIFICATION_TIMEOUT)
			return;
		}

		this.props.confirmPassword(this.state.pass1, this.state.email, this.state.key)
	}

	componentDidMount() {
		let { key, email } = url.parse(this.props.location.search, true).query;
		this.setState({ key, email })
	}

	render() {
		if (this.props.pwResetComplete) {
			return <Redirect to='/' />;
		}

		let passwordEqualError = this.state.pass2 !== this.state.pass1;
		
		return (
			<div className='login-form'>
				<Grid
					textAlign='center'
					style={{height: '100%'}}
					verticalAlign='top'
				>
					<Grid.Column style={{maxWidth: 450}}>
						<Form size='large' onSubmit={()=>this.onSubmit()}>
							<Segment>
								<Image src='images/metis-logo.png'/>
								<h2>Confirm your new password</h2>
								<Form.Input
									fluid
									type={"password"}
									icon='user'
									iconPosition='left'
									placeholder='New Password'
									name="pass1"
									onChange={this.handlePass1Change}
								/>
								<Form.Input
									fluid
									type={"password"}
									icon='user'
									iconPosition='left'
									placeholder='Confirm Password'
									name="pass2"
									error={passwordEqualError}
									onChange={this.handlePass2Change}
								/>
								<Form.Button fluid size={'large'} color={'blue'} type={"submit"}>Reset
									Password</Form.Button>
								<Link to={'/'}>
									<small>Try logging in again?</small>
								</Link>
							</Segment>
						</Form>
					</Grid.Column>
				</Grid>
			</div>
		);
	}
}


const mapStateToProps = state => (
	{ ...state.login }
);

const mapDispatchToProps = dispatch => (
	{
		confirmPassword: (password, email, key) => {dispatch(confirmPassword(password, email, key))}
	}
);

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmPassword)