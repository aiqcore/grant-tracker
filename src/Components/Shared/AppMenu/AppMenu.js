import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Icon, Layout } from 'antd';

import { USER_OPTIONS, ADMIN_OPTIONS } from './menuOptions';

import './AppMenu.css';

export default function AppMenu({ menuOpen, navigateTab, menuType }) {
    let menuOptions = [];
    let defaultSelectedKey = null;
    let optionParams = null;

    if (menuType === 'user') {
        optionParams = USER_OPTIONS;
    } else {
        optionParams = ADMIN_OPTIONS;
    }

    for (let option of optionParams) {
        if (option.defaultSelectedKey) {
            defaultSelectedKey = option.key;
        }

        menuOptions.push(
            <Menu.Item key={option.key}>
                <Icon type={option.iconType} />
                <span className="nav-text">{option.text}</span>
            </Menu.Item>
        )
    }
    
	return (
		<Layout.Sider trigger={null} collapsible collapsed={!menuOpen}>
			<h1 className="menu-header">{menuOpen ? "GrantTracker" : "GT" }</h1>
			<Menu theme="dark" mode="inline" defaultSelectedKeys={[defaultSelectedKey]} onClick={(e) => navigateTab(e.key)}>
				{ menuOptions }
			</Menu>
		</Layout.Sider>
	)
}

AppMenu.propTypes = {
	menuOpen: PropTypes.bool.isRequired,
    navigateTab: PropTypes.func.isRequired,
    menuType: PropTypes.oneOf(['user', 'admin'])
}