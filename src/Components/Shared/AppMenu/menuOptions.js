export const USER_OPTIONS = [
    {
        'key': 'overview',
        'iconType': 'area-chart',
        'text': 'Grant Overviews',
        'defaultSelectedKey': true
    }
];

export const ADMIN_OPTIONS = [
    {
        'key': 'manage',
        'iconType': 'user',
        'text': 'Manage',
        'defaultSelectedKey': true
    },
    {
        'key': 'inventory',
        'iconType': 'contacts',
        'text': 'Inventory'
    }
]