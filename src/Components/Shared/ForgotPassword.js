import React, { Component } from 'react';
import { Grid,Form,Segment,Image } from 'semantic-ui-react';
import { Spin, Icon } from 'antd';
import { Actions, sendPwResetRequest } from '../../state/actions/loginActions';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const { handleEmailChange } = Actions;

class forgotPassword extends Component {

	pwReset = () => {
		this.props.pwReset(this.props.email, window.location.href.substring(0, window.location.href.indexOf('#')))
	};

	render() {
		return (
			<div className='login-form'>
				<Grid
					textAlign='center'
					style={{ height: '100%' }}
					verticalAlign='top'
					className="login-box"
				>
					<Grid.Column style={{ maxWidth: 450 }}>
						<Form size='large'>
							<Segment >
								<Image src='images/metis-logo.png'/>
								<h2>Login to your account</h2>
								<Form.Input
									fluid
									icon='user'
									iconPosition='left'
									placeholder='Username'
									name="username"
									onChange={(e) => this.props.handleEmailChange(e.target.value)}
								/>
								<Form.Button fluid size={'large'} color={'blue'} 
									disabled={!this.props.email}
									onClick={this.pwReset}
									style={{height: '50px' }}>
										{ !this.props.pwResetLoading ? 
										"Reset Password" : 
										<Spin size="small" indicator={
											<Icon type="loading" style={{ fontSize: 24, color: 'white' }} spin />} /> 
										}
								</Form.Button>
								<Link to={'/'}><small>Try logging in again?</small></Link>
							</Segment>
						</Form>
					</Grid.Column>
				</Grid>
			</div>
		);
	}
	
};


const mapStateToProps = state => (
	{
		...state.login
	}
);

const mapDispatchToProps = dispatch => (
	{
		pwReset: (email, url) => { dispatch(sendPwResetRequest(email, url)) },
		handleEmailChange: (email) => { dispatch(handleEmailChange(email)) }
	}
);

export default connect(mapStateToProps, mapDispatchToProps)(forgotPassword)