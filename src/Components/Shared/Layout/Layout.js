import React from 'react';
import PropTypes from 'prop-types';

export default function Layout({ title, description }) {
    return (
        <div>
            <h1 className="page-header">{title}</h1>
            <p className="page-description">{description}</p>
        </div>
    ); 
}

Layout.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
};