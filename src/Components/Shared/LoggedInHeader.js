import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { logout } from '../../state/actions/loginActions';
import { Actions } from '../../state/actions/menuActions';
import { Layout, Icon } from 'antd';
import AppMenuControl from './AppMenuControl/AppMenuControl';
import './LoggedInHeader.css';

const { Header } = Layout;

class LoggedInHeader extends Component {
    render() {
        let user = this.props.login.user;
        return (
                <Header style={{background: '#fff', padding: 0 }}>
                    <AppMenuControl menuOpen={this.props.menuOpen} openMenu={this.props.openMenu} closeMenu={this.props.closeMenu} />
                    <img className="metis-logo" alt="Metis Logo" src={"images/metis-icon.png"} />
                    <span className="metis-header">Metis Foundation</span>
                    <div className="logout-button" onClick={() => this.props.logout(this.props.history)}>
                        <Icon className="logout-icon" type="logout"/>
                    </div>
                    { user ? 
                    <span className="profile-container">
                        <span className="user-text">{ `${user['First Name']} ${user['Last Name']}` }</span>
                        <img className="user-image" src={ user.admin === 1 ? "admin-icon.png" : "user-icon.png"} alt="User" />
                    </span> : null }
                </Header>
        )
    }
}


const mapStateToProps = state => {
    return {
        login: state.login,
        menuOpen: state.menu.menuOpen
    }
};

const mapDispatchToProps = dispatch => ({
    logout: (history) => { dispatch(logout(history)) },
    openMenu: () => { dispatch(Actions.openMenu()) },
    closeMenu: () => { dispatch(Actions.closeMenu()) }
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoggedInHeader));