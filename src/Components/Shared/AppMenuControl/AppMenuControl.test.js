import React from 'react';
import Enzyme, { mount } from 'enzyme'; 
import renderer from 'react-test-renderer';
import Adapter from 'enzyme-adapter-react-16';
import AppMenuControl from './AppMenuControl';

Enzyme.configure({ adapter: new Adapter() });

describe('AppMenuControl', () => {
    it('renders correctly', () => {
        const tree = renderer.create(
            <AppMenuControl openMenu={console.log} closeMenu={console.log} />
        ).toJSON();
        expect(tree).toMatchSnapshot();
    });
    it ('should show unfold when menu is closed.', () => {
        const wrapper = mount(
            <AppMenuControl openMenu={console.log} menuOpen={false} closeMenu={console.log} />
        );
        expect(wrapper.html()).toEqual('<span class="menu-control"><i class="anticon anticon-menu-unfold"></i></span>');
        wrapper.unmount();
    });
    it ('should show fold when menu is open.', () => {
        const wrapper = mount(
            <AppMenuControl openMenu={console.log} menuOpen={true} closeMenu={console.log} />
        );
        expect(wrapper.html()).toEqual('<span class="menu-control"><i class="anticon anticon-menu-fold"></i></span>');
        wrapper.unmount();
    });
});