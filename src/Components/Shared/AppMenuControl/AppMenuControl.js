import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'antd/lib/icon';

export default function AppMenuControl({ menuOpen, openMenu, closeMenu }) {
    return (
        <span className="menu-control">
            { menuOpen ?
            <Icon type="menu-fold" onClick={closeMenu}/> :
            <Icon type="menu-unfold" onClick={openMenu}/> }
        </span>
    )
}

AppMenuControl.propTypes = {
    menuOpen: PropTypes.bool.isRequired,
    openMenu: PropTypes.func.isRequired,
    closeMenu: PropTypes.func.isRequired
}

AppMenuControl.defaultProps = {
    menuOpen: true
}