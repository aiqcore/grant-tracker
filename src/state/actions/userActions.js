import {BASE_API_URL, axiosWithToken, NOTIFICATION_TIMEOUT} from '../config';
import { NotificationManager } from 'react-notifications';
import { createActions } from 'reduxsauce';
import {GRANTS_ENDPOINT} from "../apiRouting";

const { Types, Creators } = createActions({
	isUpdatingProfile: null,
	updateDirtyUser: ['field', 'value'],
	updateLoggedInUser: ['user'],
	isFetchingGrants: null,
	grantsFetched: ['grants'],
	changeSelectedGrant: ['grantIndex']
});

export const updateProfile = (user) => {
    return dispatch => {
	    dispatch(Creators.isUpdatingProfile());
	    let id = sessionStorage.getItem("userid");
	    user = {...user, id};
        axiosWithToken.put(`${BASE_API_URL}/update-account`, user).then((response) => {
        	if(response.status === 200) {
		        NotificationManager.success('Profile Updated', 'Successfully completed.', NOTIFICATION_TIMEOUT)
		        dispatch(Creators.updateLoggedInUser(user))
	        }
        }).catch((err) => {
            NotificationManager.error('Profile Update ERR', 'Could not update your profile. If persistent, please contact administrator', NOTIFICATION_TIMEOUT);
        });
    }
};

export const fetchGrants = (userID) => {
	return dispatch => {
		dispatch(Creators.isFetchingGrants());
		axiosWithToken.get(`${GRANTS_ENDPOINT}/user/${userID}`).then((response) => {
			dispatch(Creators.grantsFetched(response.data));
		}).catch((err) => {
				NotificationManager.error('There was an error fetching the grants.', 'Error!', NOTIFICATION_TIMEOUT)
			}
		);
	}
};

export { Types, Creators as Actions };