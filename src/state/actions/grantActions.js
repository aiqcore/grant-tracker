import { axiosWithToken, NOTIFICATION_TIMEOUT, NOTIFICATION_ERROR } from '../config'
import { NotificationManager } from "react-notifications";
import { createActions } from 'reduxsauce';
import { GRANTS_ENDPOINT, USERS_ENDPOINT } from "../apiRouting";

const { Types, Creators } = createActions({
    isFetchingInstitutions: null,
    fetchInstitutionsSuccess: ['institutions'],
    fetchInstitutionsError: null,
    isFetchingInvestigators: null,
    fetchInvestigatorsSuccess: ['investigators'],
    fetchInvestigatorsError: null
});

export const fetchInstitutions = () => (
    dispatch => {
        dispatch(Creators.isFetchingInstitutions());
        axiosWithToken.get(`${GRANTS_ENDPOINT}/institutions`)
            .then((response) => {
                dispatch(Creators.fetchInstitutionsSuccess(response.data));
            })
            .catch((err) => {
                NotificationManager.error('Could not fetch institutions.', NOTIFICATION_ERROR, NOTIFICATION_TIMEOUT);
                dispatch(Creators.fetchInstitutionsError());
            })
    }
);

export const fetchInvestigators = () => (
	dispatch => {
		dispatch(Creators.isFetchingInvestigators());
		axiosWithToken.get(`${USERS_ENDPOINT}`)
			.then((response) => {
				let data = response.data.filter(user => user.admin === 0);
				dispatch(Creators.fetchInvestigatorsSuccess(data));
			})
			.catch((err) => {
				NotificationManager.error('Unable to pull investigator list.', NOTIFICATION_ERROR, NOTIFICATION_TIMEOUT);
				dispatch(Creators.fetchInvestigatorsError())
			})
	}
);

export { Types, Creators as Actions };