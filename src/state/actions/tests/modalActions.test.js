
import { Types, Actions } from '../modalActions';

describe('Modal Actions', () => {
    it('creates an action to show the user modal', () => {
        const expectedAction = {
            type: Types.SHOW_USER_MODAL
        };
        expect(Actions.showUserModal()).toEqual(expectedAction);
    });

    it('creates an action to show the grant modal', () => {
        const expectedAction = {
            type: Types.SHOW_GRANT_MODAL
        };
        expect(Actions.showGrantModal()).toEqual(expectedAction);
    });

    it('creates an action to show the transaction modal', () => {
        const expectedAction = {
            type: Types.SHOW_TRANSACTION_MODAL
        };
        expect(Actions.showTransactionModal()).toEqual(expectedAction);
    });

    it('creates an action to close all modals', () => {
        const expectedAction = {
            type: Types.CLOSE_MODALS
        };
        expect(Actions.closeModals()).toEqual(expectedAction);
    });
});