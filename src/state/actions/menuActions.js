import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
    closeMenu: null,
    openMenu: null,
    navigateTab: ['tabKey']
});

export { Types, Creators as Actions };