import { axiosWithToken, NOTIFICATION_TIMEOUT, NOTIFICATION_SUCCESS, NOTIFICATION_ERROR } from '../config'
import { NotificationManager } from "react-notifications";
import { createActions } from 'reduxsauce';
import { REGISTER_ENDPOINT, EXPENSES_ENDPOINT, USERS_ENDPOINT, GRANTS_ENDPOINT } from "../apiRouting";

const { Types, Creators } = createActions({
	isRegisteringUser: null,
	registeringUserComplete: null,
	registeringUserError: null,
	clearPreviousUserRegistration: null,
	clearPreviousGrantRegistration: null,
	isFetchingAllUsers: null,
	fetchedAllUsers: ['userList'],
	allUserFetchErr: null,
	fetchUser: ['id'],
	isRegisteringGrant: null,
	registeringGrantComplete: null,
	clearPreviousRegistration: null,
	registeringGrantError: null,
	isFetchingUser: null,
	isFetchingAllGrants: null,
	fetchGrantsSuccess: ['grants'],
	isUpdatingGrant: null,
	grantUpdated: null,
	isCreatingExpense: null,
	createExpenseSuccess: null,
	isFetchingAllExpenses: null,
	fetchAllExpensesSuccess: ['expenses'],
	fetchGrantSuccess: ['selectedGrantDetails'],
	isFetchingSalutations: null,
	fetchSalutationsSuccess: ['salutations']
});

export const registerUser = (user) => {
	return dispatch => {
		dispatch(Creators.isRegisteringUser());
		axiosWithToken.post(REGISTER_ENDPOINT, {
            userDetails: user
    	})
			.then((response) => {
				if (response.status === 200) {
					NotificationManager.success(`The user with email ${user.email} has been created.`, NOTIFICATION_SUCCESS, NOTIFICATION_TIMEOUT);
					dispatch(Creators.registeringUserComplete());
				}
			})
			.catch((err) => {
				if(err.response.status === 401) {
					NotificationManager.error('An account with that e-mail is already registered.', NOTIFICATION_ERROR, NOTIFICATION_TIMEOUT);
				}
				else {
					NotificationManager.error('Unable to create user.', NOTIFICATION_ERROR, NOTIFICATION_TIMEOUT);
				}
				dispatch(Creators.registeringUserError())
			});
	}
};

export const registerGrant = (grant) => {
	grant = {
		...grant
	};

	return dispatch => {
		dispatch(Creators.isRegisteringGrant());
		axiosWithToken.post(`${GRANTS_ENDPOINT}`, grant)
			.then((response) => {
				if(response.status === 200) {
					NotificationManager.success('Grant successfully created.', NOTIFICATION_SUCCESS, NOTIFICATION_TIMEOUT);
					dispatch(Creators.registeringGrantComplete());
				}
			})
			.catch((err) => {
				NotificationManager.error('Grant was unable to be created.', NOTIFICATION_ERROR, NOTIFICATION_TIMEOUT);
				dispatch(Creators.registeringGrantError())
			});
	}
};

export const fetchSalutations = () => (
	dispatch => {
		dispatch(Creators.isFetchingSalutations);
		axiosWithToken.get(`${USERS_ENDPOINT}/salutations`)
		.then((response) => {
			dispatch(Creators.fetchSalutationsSuccess(response.data));
		})
		.catch(err => NotificationManager.error('Could not fetch salutations.', NOTIFICATION_ERROR, NOTIFICATION_TIMEOUT));
	}
)

export const fetchAllUsers = () => (
	dispatch => {
		dispatch(Creators.isFetchingAllUsers());
		axiosWithToken.get(`${USERS_ENDPOINT}`)
			.then((response) => {
				if(response.data && response.data.length > 0 && response.status === 200)
					dispatch(Creators.fetchedAllUsers(response.data));
				else {
					NotificationManager.error('Received an empty user list from the server.', NOTIFICATION_ERROR, NOTIFICATION_TIMEOUT);
				}
			})
			.catch((err) => {
				NotificationManager.error('Unable to fetch user list.',`Error!`, NOTIFICATION_TIMEOUT);
				dispatch(Creators.allUserFetchErr());
			});
	}
);

export const updateGrant = (grant) => (
	dispatch => {
		dispatch(Creators.isUpdatingGrant());
		axiosWithToken.put(`${GRANTS_ENDPOINT}/${grant['id']}`, grant)
			.then((response) => {
				NotificationManager.success('Grant Updated', `Grant with id #${grant['id']} updated`, NOTIFICATION_TIMEOUT);
				dispatch(Creators.grantUpdated());
			})
			.catch(function(err) {
				NotificationManager.error('Error!', 'Unable to update grant', NOTIFICATION_TIMEOUT);
			})
	}
);

export const updateUser = (userID, user) => (
    dispatch => {
        axiosWithToken.put(`${USERS_ENDPOINT}/${userID}`, user)
            .then(() => {
                NotificationManager.success('User updated successfully.', 'Success!', NOTIFICATION_TIMEOUT);
            })
            .catch(() => {
                NotificationManager.error('Unable to edit user.', 'Error!', NOTIFICATION_TIMEOUT);
            })
    }
);

export const fetchGrant = (grantID) => (
	dispatch => {
		axiosWithToken.get(`${GRANTS_ENDPOINT}/${grantID}`)
			.then((response) => {
				dispatch(Creators.fetchGrantSuccess(response.data));
			})
			.catch(() => {
				NotificationManager.error('Unable to fetch grant details.', 'Error!', NOTIFICATION_TIMEOUT);
			})
	}
);

export const deleteGrant = (grantID) => (
	dispatch => {
		axiosWithToken.delete(`${GRANTS_ENDPOINT}/${grantID}`)
			.then(() => {
			NotificationManager.success('Grant deleted successfully.', 'Success!', NOTIFICATION_TIMEOUT);
		})
			.catch(() => {
				NotificationManager.error('Unable to delete grant.', 'Error!', NOTIFICATION_TIMEOUT);
			})
	}
);

export const fetchAllGrants = () => (
    dispatch => {
        dispatch(Creators.isFetchingAllGrants());
        axiosWithToken.get(`${GRANTS_ENDPOINT}`)
            .then((response) => {
                dispatch(Creators.fetchGrantsSuccess(response.data));
            })
            .catch(function (err) {
                NotificationManager.error('Unable to fetch grants.', 'Error!', NOTIFICATION_TIMEOUT);
            })
    }
);

export const deleteUser = (userID) => (
    dispatch => {
        axiosWithToken.delete(`${USERS_ENDPOINT}/${userID}`)
            .then(() => {
                NotificationManager.success('User deleted successfully.', 'Success!', NOTIFICATION_TIMEOUT);
            })
            .catch(() => {
                NotificationManager.error('Unable to delete user. Please try again.', 'Error!', NOTIFICATION_TIMEOUT);
            })
    }
);

export const createExpense = (expense) => (
	dispatch => {
        dispatch(Creators.isCreatingExpense());
		axiosWithToken.post(`${EXPENSES_ENDPOINT}`, expense)
			.then((response) => {
				NotificationManager.success('Expense successfully created.', 'Success!', NOTIFICATION_TIMEOUT);
			})
			.catch(function (err) {
				NotificationManager.error('Unable to create transaction.', 'Error!', NOTIFICATION_TIMEOUT);
			})
	}
);

export const deleteExpense = (expenseID) => (
	dispatch => {
		axiosWithToken.delete(`${EXPENSES_ENDPOINT}/${expenseID}`)
			.then((response) => {
				NotificationManager.success('Expense deleted successfully.', 'Success!', NOTIFICATION_TIMEOUT);
			})
			.catch(function (err) {
				NotificationManager.error('Unable to delete expense. Please try again.', 'Error!', NOTIFICATION_TIMEOUT);
			})
	}
);

export const updateExpense = (expenseID, expense) => (
	dispatch => {
		axiosWithToken.put(`${EXPENSES_ENDPOINT}/${expenseID}`, expense)
			.then((response) => {
				NotificationManager.success('Expense updated successfully.', 'Success!', NOTIFICATION_TIMEOUT);
			})
			.catch(function (err) {
				NotificationManager.error('Unable to edit expense.', 'Error!', NOTIFICATION_TIMEOUT);
			})
	}
);

export const fetchAllExpenses = () => (
	dispatch => {
		dispatch(Creators.isFetchingAllExpenses());
		axiosWithToken.get(`${EXPENSES_ENDPOINT}`)
			.then((response) => {
				dispatch(Creators.fetchAllExpensesSuccess(response.data));
			})
			.catch(function (err) {
				NotificationManager.error('Unable to fetch expenses.', 'Error!', NOTIFICATION_TIMEOUT);
			})
	}
);


export { Types, Creators as Actions };