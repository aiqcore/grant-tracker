import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
    showUserModal: null,
    showGrantModal: null,
    showTransactionModal: null,
    closeModals: null
});

export { Types, Creators as Actions };