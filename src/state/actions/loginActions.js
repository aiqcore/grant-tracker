import { BASE_API_URL, axiosNoToken, axiosWithToken, NOTIFICATION_TIMEOUT, NOTIFICATION_ERROR, NOTIFICATION_SUCCESS } from '../config';
import { createActions } from 'reduxsauce';
import { NotificationManager } from "react-notifications";
import { USERS_ENDPOINT } from '../apiRouting';

const { Types, Creators } = createActions({
	isLoggingIn: null,
	isLoggedIn: ['access_token', 'user'],
	loginError: null,
	tokenBad: null,
	handleEmailChange: ['email'],
	handlePwChange: ['pw'],
	confirmingNewPassword: null,
	newPasswordConfirmed: null,
	userLogout: null,
	pwResetLoading: null,
	pwResetComplete: null
});

export const login = (username, password) => {
    return dispatch => {
        dispatch(Creators.isLoggingIn());
		axiosNoToken.post(`${BASE_API_URL}/login`, {}, 
			{
				auth: {
					username,
					password
				}
			})
		.then((response) => {
			sessionStorage.setItem("access_token",response.data.access_token);
			sessionStorage.setItem("userid", response.data.user["Client ID"]);
			dispatch(Creators.isLoggedIn(response.data.access_token, response.data.user));
		})
		.catch(() => {
			NotificationManager.error('Please check your credentials.', NOTIFICATION_ERROR, NOTIFICATION_TIMEOUT);
			dispatch(Creators.loginError());
		});
    }
};

export const alreadyLoggedIn = () => {
    return dispatch => {
        let id = sessionStorage.getItem("userid");
        let token = sessionStorage.getItem("access_token");
	    dispatch(Creators.isLoggingIn());
        axiosWithToken.get(`${USERS_ENDPOINT}/${id}`)
            .then((response) => {
                if(response.status === 200) {
                    dispatch(Creators.isLoggedIn(token, response.data))
                }
            })
            .catch((err) => {
            	if(err.response.data === 'Bad Token') {
            		NotificationManager.error('Session expired, please log in again.', NOTIFICATION_ERROR, NOTIFICATION_TIMEOUT);
					dispatch(Creators.tokenBad())
	            }
				else {
					NotificationManager.error('Could not fetch user profile data.', NOTIFICATION_ERROR, NOTIFICATION_TIMEOUT)
					dispatch(Creators.tokenBad());
				}
            });
    }
};

export const sendPwResetRequest = (email, url) => {
    return dispatch => {
		dispatch(Creators.pwResetLoading());
	    axiosNoToken
		    .post(`${BASE_API_URL}/forgot-password`, {'user_name': email, url})
		    .then((response) => {
				dispatch(Creators.pwResetComplete());
			    NotificationManager.success('Reset Request Received', 'An email should arrive in your inbox shortly', NOTIFICATION_TIMEOUT);
		    })
		    .catch(err => {
				dispatch(Creators.pwResetComplete());
			    NotificationManager.error('Could not find the requested account.', NOTIFICATION_ERROR, NOTIFICATION_TIMEOUT);
		    });
    }
};

export const confirmPassword = (password, email, key) => (
	dispatch => {
		dispatch(Creators.confirmingNewPassword())
		axiosNoToken
			.post(`${BASE_API_URL}/forgot-password-confirm/${key}`, {email, password})
			.then((response) => {
				NotificationManager.success('Password reset successfully.', NOTIFICATION_SUCCESS, NOTIFICATION_TIMEOUT);
				dispatch(Creators.pwResetComplete());
			})
			.catch(err => {
				if(err.response.data === 'Invalid Password')
					NotificationManager.error('Password Reset ERR', 'Password must be 8 or more characters and contain at least one number', NOTIFICATION_TIMEOUT);
				else if(err.response.data === "Bad Reset Code")
					NotificationManager.error('Password Reset ERR', 'Reset session has expired. Please request a new password reset using the forgot password feature.', NOTIFICATION_TIMEOUT);
				else
					NotificationManager.error('Password Reset ERR', 'Error resetting password', NOTIFICATION_TIMEOUT);

			});
	}
);

export const logout = (history) => (
	dispatch => {
		sessionStorage.clear();
		history.push('/');
		dispatch(Creators.userLogout());
	}
);


export { Types, Creators as Actions };
