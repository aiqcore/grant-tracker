import { BASE_API_URL } from './config';

export const REGISTER_ENDPOINT = `${BASE_API_URL}/register`;

export const USERS_ENDPOINT = `${BASE_API_URL}/users`;
export const GRANTS_ENDPOINT = `${BASE_API_URL}/grants`;
export const EXPENSES_ENDPOINT = `${BASE_API_URL}/expenses`;