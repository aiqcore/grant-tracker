import { Types } from '../actions/userActions';

// INITIALIZE STATE

const initialState = {
    isUpdatingUser: false,
    userUpdated:false,
    userUpdateError:false,
    grantsFetched: false,
    grantsFetchError: false,
    isFetchingGrants: false,
    grants: [],
    dirtyUser: {},
    grantSelectedIndex: 0
};


// REDUCER

export default (state = initialState, action) => {
    switch (action.type) {
        case Types.IS_UPDATING_USER:
            return {
                ...state,
                isUpdatingUser: true
            };
        case Types.USER_UPDATED:
            return {
                ...state,
                isUpdatingUser: false
            };
        case Types.CLEAR_USER_UPDATED:
            return {
                ...state,
                userUpdated: false,
                userUpdateError: false
            };
        case Types.USER_UPDATE_ERROR:
            return {
                ...state,
                userUpdateError: true,
                isUpdatingUser: false
            };
        case Types.CLEAR_USER_UPDATED_ERROR:
            return {
                ...state,
                userUpdateError: false
            };
        case Types.GRANTS_FETCHED:
            return {
                ...state,
                grantsFetchError: false,
                isFetchingGrants: false,
                grants: action.grants
            };
        case Types.GRANTS_FETCH_ERROR:
            return {
                ...state,
                grantsFetchError: true,
                isFetchingGrants: false
            };
        case Types.IS_FETCHING_GRANTS:
            return {
                ...state,
                isFetchingGrants: true,
                grantsFetchError: false
            };
        case Types.UPDATE_DIRTY_USER:
            return {
                ...state,
                dirtyUser: {
                    ...state.dirtyUser,
                    [action.field]: action.value
                }
            };
        case Types.CHANGE_SELECTED_GRANT:
            return {
                ...state,
                grantSelectedIndex: action.grantIndex
            };
        default:
            return state;
    }
};