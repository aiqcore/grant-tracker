import { Types } from '../actions/grantActions';

const initialState = {
    isFetchingInstitutions: false,
    institutions: [],
    isFetchingInvestigators: false,
    investigators: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Types.IS_FETCHING_INSTITUTIONS:
            return {
                ...state,
                isFetchingInstitutions: true
            }
        case Types.FETCH_INSTITUTIONS_SUCCESS:
            return {
                ...state,
                institutions: action.institutions,
                isFetchingInstitutions: false
            }
        case Types.FETCH_INSTITUTIONS_ERROR:
            return {
                ...state,
                isFetchingInstitutions: false
            }
        case Types.IS_FETCHING_INVESTIGATORS:
			return {
				...state,
				isFetchingInvestigators: true
			};
		case Types.FETCH_INVESTIGATORS_SUCCESS:
			return {
				...state,
				isFetchingInvestigators: false,
				investigators: action.investigators
			};
		case Types.FETCH_INVESTIGATORS_ERROR:
			return {
                ...state,
                isFetchingInvestigators: false
			};
        default:
            return state;
    }
}

