import { Types } from '../actions/adminActions';

const initialState = {
	isFetchingGrants: false,
	isRegisteringUser: false,
	registrationErr: false,
	isFetchingAllUsers: false,
	isCreatingGrant: false,
	grantCreationError: false,
	isUpdatingGrant: false,
	userRegistered: false,
	users: [],
	grants: [],
	expenses: [],
	salutations: []
};

export default (state = initialState, action) => {
	switch (action.type) {
		case Types.IS_REGISTERING_USER:
			return {
				...state,
				isRegisteringUser: true
			};
		case Types.REGISTERING_USER_COMPLETE:
			return {
				...state,
				userRegistered: true,
				isRegisteringUser: false
			};
		case Types.REGISTERING_USER_ERROR:
			return {
				...state,
				registrationErr: true,
				isRegisteringUser: false,
				userRegistered: false
			};
		case Types.IS_FETCHING_ALL_USERS:
			return {
				...state,
				isFetchingAllUsers: true
			};
		case Types.FETCHED_ALL_USERS:
			return {
				...state,
				users: action.userList,
				isFetchingAllUsers: false
			};
		case Types.ALL_USER_FETCH_ERR:
			return {
				...state,
				isFetchingAllUsers: false,
				fetchAllUsersErr: true
			};
		case Types.FETCH_USER:
			for (let i = 0; i < state.users.length;i++){
				if(state.users[i]["id"] === action.id){
					return {
						...state,
						selectedUser: {...state.users[i]}
					}
				}
			}
			return state;
		case Types.IS_FETCHING_ALL_GRANTS:
			return {
				...state,
				isFetchingAllGrants: true
			};
		case Types.FETCH_GRANTS_SUCCESS:
			return {
				...state,
				isFetchingAllGrants: false,
				grants: action.grants
			};
		case Types.IS_UPDATING_GRANT:
			return {
				...state,
				isUpdatingGrant: true
			};
		case Types.GRANT_UPDATED:
			return {
				...state,
				isUpdatingGrant: false
			};
		case Types.IS_CREATING_EXPENSE:
			return {
				...state,
				isCreatingExpense: true
			};
		case Types.CREATE_EXPENSE_SUCCESS:
			return {
				...state,
				isCreatingExpense: false
			};
		case Types.IS_FETCHING_ALL_EXPENSES:
			return {
				...state,
				isFetchingAllExpenses: true
			};
		case Types.FETCH_ALL_EXPENSES_SUCCESS:
			return {
				...state,
				expenses: action.expenses,
				isFetchingAllExpenses: false
			};
		case Types.FETCH_GRANT_SUCCESS:
			return {
				...state,
				selectedGrantDetails: action.selectedGrantDetails
			}
		case Types.IS_FETCHING_SALUTATIONS:
			return {
				...state,
				isFetchingSalutations: true
			}
		case Types.FETCH_SALUTATIONS_SUCCESS:
			return {
				...state,
				isFetchingSalutations: false,
				salutations: action.salutations
			}
		default:
			return state;
	}
};