import { combineReducers } from 'redux';
import login from './loginReducer';
import user from './userReducer';
import admin from './adminReducer';
import menu from './menuReducer';
import modal from './modalReducer';
import grant from './grantReducer';

import { Types } from '../actions/loginActions';

const composedReducer = combineReducers({
    login, user, admin, menu, modal, grant
});


export const rootReducer = (state, action) => {
    if(action.type === Types.USER_LOGOUT) {
        state = undefined;
    }

    return composedReducer(state, action);
};