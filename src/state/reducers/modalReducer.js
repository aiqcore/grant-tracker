import { createReducer } from 'reduxsauce';
import { Types } from '../actions/modalActions';

const INITIAL_STATE = {
    userModalVisible: false,
    grantModalVisible: false,
    transactionModalVisible: false
};

export const showUserModal = (state = INITIAL_STATE) => {
    return { ...state, userModalVisible: true, grantModalVisible: false, transactionModalVisible: false };
};

export const showGrantModal = (state = INITIAL_STATE) => {
    return { ...state, userModalVisible: false, grantModalVisible: true, transactionModalVisible: false };
};

export const showTransactionModal = (state = INITIAL_STATE) => {
    return { ...state, userModalVisible: false, grantModalVisible: false, transactionModalVisible: true };
};

export const closeModals = () => {
    return INITIAL_STATE;
};

export const HANDLERS = {
    [Types.SHOW_USER_MODAL]: showUserModal,
    [Types.SHOW_GRANT_MODAL]: showGrantModal,
    [Types.SHOW_TRANSACTION_MODAL]: showTransactionModal,
    [Types.CLOSE_MODALS]: closeModals
};

export default createReducer(INITIAL_STATE, HANDLERS);
