import { createReducer } from 'reduxsauce';
import { Types } from '../actions/loginActions';

const INITIAL_STATE = {
    access_token: '',
    user: null,
    redirectAdmin: false,
    redirectUser: false,
    isLoggingIn: false,
    isLoggedIn: false,
    tokenBad: false,
    pwResetComplete: false
};

export const isLoggingIn = (state = INITIAL_STATE) => {
    return { ...state, isLoggingIn: true };
};

export const isLoggedIn = (state = INITIAL_STATE, action) => {
    return { 
        ...state, 
        isLoggingIn: false, 
        isLoggedIn: true,
        redirectAdmin: action.user.admin === 1,
        redirectUser: action.user.admin !== 1,
        access_token: action.access_token,
        user: action.user
    }
}

export const loginError = (state = INITIAL_STATE) => {
    return { ...state, isLoggingIn: false, loginError: true };
};

export const handleEmailChange = (state = INITIAL_STATE, action) => {
    return { ...state, email: action.email };
};

export const handlePwChange = (state = INITIAL_STATE, action) => {
    return { ...state, pw: action.pw };
};

export const pwResetLoading = (state = INITIAL_STATE) => {
    return { ...state, pwResetLoading: true };
};

export const pwResetComplete = (state = INITIAL_STATE) => {
    return { ...state, pwResetLoading: false, pwResetComplete: true };
};

export const HANDLERS = {
    [Types.IS_LOGGING_IN]: isLoggingIn,
    [Types.IS_LOGGED_IN]: isLoggedIn,
    [Types.LOGIN_ERROR]: loginError,
    [Types.HANDLE_EMAIL_CHANGE]: handleEmailChange,
    [Types.HANDLE_PW_CHANGE]: handlePwChange,
    [Types.PW_RESET_LOADING]: pwResetLoading,
    [Types.PW_RESET_COMPLETE]: pwResetComplete
}

export default createReducer(INITIAL_STATE, HANDLERS);
