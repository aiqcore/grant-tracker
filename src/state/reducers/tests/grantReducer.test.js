import grantReducer from '../grantReducer';
import { Types } from '../../actions/grantActions';

const initialState = {
    isFetchingInstitutions: false,
    institutions: [],
    isFetchingInvestigators: false,
    investigators: []
};

describe('Grant Reducer', () => {
    it('should have an initial state', () => {
        expect(grantReducer(undefined, { type: 'unexpected' })).toEqual(initialState);
    });

    it('should be able to load the institution list', () => {
        expect(grantReducer(undefined, { 
            type: Types.IS_FETCHING_INSTITUTIONS 
        })).toEqual({ ...initialState, isFetchingInstitutions: true });
    });

    it('should be able to set the institution list', () => {
        expect(grantReducer(undefined, {
            type: Types.FETCH_INSTITUTIONS_SUCCESS,
            institutions: ['Georgia Tech', 'MIT']
        })).toEqual({ ...initialState, isFetchingInstitutions: false, institutions: ['Georgia Tech', 'MIT'] })
    });

    it('should be able to throw an error on institution fetching', () => {
        expect(grantReducer(undefined, {
            type: Types.FETCH_INSTITUTIONS_ERROR
        })).toEqual({ ...initialState, isFetchingInstitutions: false });
    });

    it('should be able to load the investigator list', () => {
        expect(grantReducer(undefined, { 
            type: Types.IS_FETCHING_INVESTIGATORS
        })).toEqual({ ...initialState, isFetchingInvestigators: true });
    });

    it('should be able to set the investigator list', () => {
        expect(grantReducer(undefined, {
            type: Types.FETCH_INVESTIGATORS_SUCCESS,
            investigators: ['Yash Gunapati', 'Austin Osborne']
        })).toEqual({ ...initialState, isFetchingInvestigators: false, investigators: ['Yash Gunapati', 'Austin Osborne'] })
    });

    it('should be able to throw an error on investigator fetching', () => {
        expect(grantReducer(undefined, {
            type: Types.FETCH_INVESTIGATORS_ERROR
        })).toEqual({ ...initialState, isFetchingInvestigators: false });
    });
});