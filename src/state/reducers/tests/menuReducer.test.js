import menuReducer from '../menuReducer';
import { Types } from '../../actions/menuActions';

const initialState = {
    menuOpen: false
};

describe('Menu Reducer', () => {
    it('has a default state', () => {
        expect(menuReducer(undefined, { type: 'unexpected' })).toEqual(initialState);
    });

    it('should be able to close the menu', () => {
        expect(menuReducer(undefined, { 
            type: Types.CLOSE_MENU 
        })).toEqual({ ...initialState, menuOpen: false });
    });

    it('should be able to open the menu', () => {
        expect(menuReducer(undefined, {
            type: Types.OPEN_MENU
        })).toEqual({ ...initialState, menuOpen: true });
    });

    it('should be able to change the open tab', () => {
        expect(menuReducer(undefined, {
            type: Types.NAVIGATE_TAB,
            tabKey: 'test tab'
        })).toEqual({ ...initialState, activeTab: 'test tab'});
    });
});