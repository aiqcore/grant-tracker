import userReducer from '../userReducer';
import { Types } from '../../actions/userActions';

const initialState = {
    isUpdatingUser: false,
    userUpdated:false,
    userUpdateError:false,
    grantsFetched: false,
    grantsFetchError: false,
    isFetchingGrants: false,
    grants: [],
    dirtyUser: {},
    grantSelectedIndex: 0
};

describe('User Reducer', () => {
    it('should have an initial state', () => {
        expect(userReducer(undefined, { type: 'unexpected' })).toEqual(initialState);
    });
});