import adminReducer from '../adminReducer';
import { Types } from '../../actions/adminActions';

const initialState = {
	isFetchingGrants: false,
	isRegisteringUser: false,
	registrationErr: false,
	isFetchingAllUsers: false,
	isCreatingGrant: false,
	grantCreationError: false,
    isUpdatingGrant: false,
    userRegistered: false,
	users: [],
	grants: [],
	expenses: [],
	salutations: []
};

describe('Admin Reducer', () => {
    it('has a default state', () => {
        expect(adminReducer(undefined, { type: 'unexpected' })).toEqual(initialState);
    });

    it('can handle user registration loading', () => {
        expect(adminReducer(undefined, {
                type: Types.IS_REGISTERING_USER
        }
        )).toEqual({ ...initialState, isRegisteringUser: true });
    });

    it('can complete user registration', () => {
        expect(adminReducer(undefined, {
            type: Types.REGISTERING_USER_COMPLETE
        })).toEqual({ ...initialState, userRegistered: true, isRegisteringUser: false });
    });

    it('can complete user registration', () => {
        expect(adminReducer(undefined, {
            type: Types.REGISTERING_USER_ERROR
        })).toEqual({ ...initialState, registrationErr: true, isRegisteringUser: false });
    });

    it('can load on fetching all users', () => {
        expect(adminReducer(undefined, {
            type: Types.IS_FETCHING_ALL_USERS
        })).toEqual({ ...initialState, isFetchingAllUsers: true });
    });

    it('can fetch all users', () => {
        expect(adminReducer(undefined, {
            type: Types.FETCHED_ALL_USERS,
            userList: ['User 1', 'User 2']
        })).toEqual({ ...initialState, users: ['User 1', 'User 2'], isFetchingAllUsers: false });
    });

    it('can throw an error on user fetch', () => {
        expect(adminReducer(undefined, {
            type: Types.ALL_USER_FETCH_ERR
        })).toEqual({ ...initialState, isFetchingAllUsers: false, fetchAllUsersErr: true });
    });

    it('can fetch an individual user', () => {
        let state = {
            ...initialState,
            users: [
                {
                    "id": 1,
                    "name": "Austin Osborne"
                },
                {
                    "id": 2,
                    "name": "Yash Gunapati"
                }
            ]
        };
        expect(adminReducer(state, {
            type: Types.FETCH_USER,
            id: 2
        })).toEqual({ ...state, selectedUser: {
            "id": 2,
            "name": "Yash Gunapati"
        }});
    });

    it('can load on fetching all grants', () => {
        expect(adminReducer(undefined, {
            type: Types.IS_FETCHING_ALL_GRANTS
        })).toEqual({ ...initialState, isFetchingAllGrants: true });
    });

    it('can set all grants upon fetch', () => {
        expect(adminReducer(undefined, {
            type: Types.FETCH_GRANTS_SUCCESS, 
            grants: ['Grant 1', 'Grant 2']
        })).toEqual({ ...initialState, isFetchingAllGrants: false, grants: ['Grant 1', 'Grant 2'] });
    });

    it('can update a grant', () => {
        expect(adminReducer(undefined, {
            type: Types.IS_UPDATING_GRANT
        })).toEqual({ ...initialState, isUpdatingGrant: true });
    });

    it('can complete a grant update', () => {
        expect(adminReducer(undefined, {
            type: Types.GRANT_UPDATED
        })).toEqual({ ...initialState, isUpdatingGrant: false });
    });

    it('can create a transaction', () => {
        expect(adminReducer(undefined, {
            type: Types.IS_CREATING_EXPENSE
        })).toEqual({ ...initialState, isCreatingExpense: true });
    });

    it('can successfully complete the creation of a transaction', () => {
        expect(adminReducer(undefined, {
            type: Types.CREATE_EXPENSE_SUCCESS
        })).toEqual({ ...initialState, isCreatingExpense: false });
    });

    it('can fetch all expenses', () => {
        expect(adminReducer(undefined, {
            type: Types.IS_FETCHING_ALL_EXPENSES
        })).toEqual({ ...initialState, isFetchingAllExpenses: true });
    });

    it('can successfully complete the fetch of all expenses', () => {
        expect(adminReducer(undefined, {
            type: Types.FETCH_ALL_EXPENSES_SUCCESS,
            expenses: ['Expense 1', 'Expense 2']
        })).toEqual({ ...initialState, isFetchingAllExpenses: false, expenses: ['Expense 1', 'Expense 2'] });
    });

    it('can fetch the grant details', () => {
        expect(adminReducer(undefined, {
            type: Types.FETCH_GRANT_SUCCESS,
            selectedGrantDetails: ['1', '2']
        })).toEqual({ ...initialState, selectedGrantDetails: ['1', '2'] });
    });

    it('can fetch all salutations', () => {
        expect(adminReducer(undefined, {
            type: Types.IS_FETCHING_SALUTATIONS
        })).toEqual({ ...initialState, isFetchingSalutations: true });
    });

    it('can successfully complete the fetch of all expenses', () => {
        expect(adminReducer(undefined, {
            type: Types.FETCH_SALUTATIONS_SUCCESS,
            salutations: ['Mr.', 'Mrs.']
        })).toEqual({ ...initialState, salutations: ['Mr.', 'Mrs.'], isFetchingSalutations: false });
    });

    it('should return the state if the user cannot be found', () => {
        let state = {
            ...initialState,
            users: [
                {
                    "id": 1,
                    "name": "Austin Osborne"
                },
                {
                    "id": 2,
                    "name": "Yash Gunapati"
                }
            ]
        };
        expect(adminReducer(state, {
            type: Types.FETCH_USER,
            id: 3
        })).toEqual(state);
    });
});