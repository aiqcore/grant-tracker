import loginReducer from '../loginReducer';
import { Types } from '../../actions/loginActions';

const initialState = {
    access_token: '',
    user: null,
    redirectAdmin: false,
    redirectUser: false,
    isLoggingIn: false,
    isLoggedIn: false,
    tokenBad: false,
    pwResetComplete: false
};

describe('Login Reducer', () => {
    it('should have an initial state', () => {
        expect(loginReducer(undefined, { type: 'unexpected' })).toEqual(initialState);
    });
});