import modalReducer from '../modalReducer';
import { Types } from '../../actions/modalActions';

const initialState = {
    userModalVisible: false,
    grantModalVisible: false,
    transactionModalVisible: false
};

describe('Modal Reducer', () => {
    it('has a default state', () => {
        expect(modalReducer(undefined, { type: 'unexpected' })).toEqual(initialState);
    });

    it('can show the user modal', () => {
        expect(modalReducer(undefined, {
                type: Types.SHOW_USER_MODAL
        }
        )).toEqual({ ...initialState, userModalVisible: true, grantModalVisible: false, transactionModalVisible: false });
    });

    it('can show the grant modal', () => {
        expect(modalReducer(undefined, {
            type: Types.SHOW_GRANT_MODAL
        })).toEqual({ ...initialState, userModalVisible: false, grantModalVisible: true, transactionModalVisible: false });
    });

    it('can show the transaction modal', () => {
        expect(modalReducer(undefined, {
            type: Types.SHOW_TRANSACTION_MODAL
        })).toEqual({ ...initialState, userModalVisible: false, grantModalVisible: false, transactionModalVisible: true });
    });

    it('can close all modals', () => {
        expect(modalReducer(undefined, {
            type: Types.CLOSE_MODALS
        })).toEqual({ ...initialState, userModalVisible: false, grantModalVisible: false, transactionModalVisible: false });
    });
});