import { createReducer } from 'reduxsauce';
import { Types } from '../actions/menuActions';

const INITIAL_STATE = {
    menuOpen: false
};

export const closeMenu = (state = INITIAL_STATE) => {
    return { ...state, menuOpen: false };
};

export const openMenu = (state = INITIAL_STATE) => {
    return { ...state, menuOpen: true };
};

export const navigateTab = (state = INITIAL_STATE, action) => {
    return { ...state, activeTab: action.tabKey };
};

export const HANDLERS = {
    [Types.CLOSE_MENU]: closeMenu,
    [Types.OPEN_MENU]: openMenu,
    [Types.NAVIGATE_TAB]: navigateTab
};

export default createReducer(INITIAL_STATE, HANDLERS);