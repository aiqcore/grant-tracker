// IMPORT PACKAGE REFERENCES

import { createStore, applyMiddleware } from 'redux';

// IMPORT MIDDLEWARE

import thunk from 'redux-thunk';
// IMPORT REDUCERS

import { rootReducer } from '../reducers/index';


// CONFIGURE STORE

export const createAppStore = () =>
(
    createStore(rootReducer, applyMiddleware(thunk))
);