import axios from 'axios';

export const BASE_API_URL = process.env.REACT_APP_BASE_API_URL;

export const EXPENSE_TYPES = ['Salaries', 'Fringe Benefits', 'Subawards', 'Materials & Supplies', 'Travel', 'Equipment', 'Publication', 'Indirect'];

export const dateFormat = 'YYYY-MM-DD';

export const axiosWithToken = axios.create();

export const axiosNoToken = axios.create();

axiosNoToken.defaults.headers.common['Content-Type'] = 'application/json';
axiosWithToken.defaults.headers.common['Content-Type'] = 'application/json';

axiosWithToken.interceptors.request
    .use(config => {
        const token = sessionStorage.getItem("access_token");
        if ( token != null ) config.headers.Authorization = `Bearer ${token}`;
        return config;
    }, err => {
        return Promise.reject(err);
    });

// Notifications
export const NOTIFICATION_TIMEOUT = 5000;
export const NOTIFICATION_ERROR = 'Error!';
export const NOTIFICATION_SUCCESS = 'Success!';

